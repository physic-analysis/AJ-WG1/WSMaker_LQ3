# Setup
You need to create the following directory.
```
- build/ 
- configs/
- inputConfigs/
- output/
- slimed_files/
```

# How to run
```
python scripts/select_slim.py -i merged.squirtle.12Feb2020.mc16ade.root -m BDTScore_single_trained,BDTScore,PNNScore
echo "TauLH 13TeV_TauLH_merged.squirtle.12Feb2020_v2.mc16ade.slimed.root" > inputConfigs/LQ3LH_v1.txt
SplitInputs -v ${version} -r Run2 -inDir ./slimed_files
python scripts/Analysis_LQ3.py -i LQ3LH_v1 -o ${output_version} --use_bdt
```
1.  Prepare the merged file created by your analysis code
2.  Slim the input file
3.  Create the config file
4.  Split the file into each b-tag region
5.  Create Workspace or calculate the SigXsecOverSM limit

## workspace 
The ```Analysis_LQ3.py``` script will call the ```WSMakerCore/scripts/doActions.py``` with the various options. 
- -w : create RooStat::WorkSpace
- -l : calculate the SigXsecOverSM by the maximum likelihood method

## output 
The output files will be located under ```output/${version}.${output_version}_StatOnly(,Syst)_```.
```
- logs : log files
- plots
- workspaces : output by RooStat
- xml 
- root-files/obs : mu limit with the ROOT and text file. This file will be used to see the cross section limit.
```


# Cross section
We use 
- https://twiki.cern.ch/twiki/bin/view/LHCPhysics/SUSYCrossSections13TeVstopsbottom#NNLOapprox_NNLL
values as the theory cross section. You don't need to take the information from this Twiki. You can use the external packages:
- https://github.com/fuenfundachtzig/xsec
Please create the working directory
```
mkdir extPackages
cd extPackages
git clone git@github.com:fuenfundachtzig/xsec.git
```

# Ranking plots
## how to draw the nuisance parameter ranking plots
- WSMakerCore/scripts/runNPranking.py
    - WSMakerCore/macros/runPulls.C
        - This script computes pulls and impact on the POI
    - WSMakerCore/macros/runBreakdown.C
- WSMakerCore/scripts/makeNPrankPlots.py
    - WSMakerCore/macros/drawPlot_pulls.C

## Rankking plots
- ```$inver.${outver}_HH_13TeV_${outver}_Systs_lephad_SM_BDT_0``` is the directory which is created in "output" folder after running workspace
    - last "0" should be the mass point

```
python WSMakerCore/scripts/runNPranking.py    $inver.${outver}_HH_13TeV_${outver}_Systs_lephad_SM_BDT_0 0
python WSMakerCore/scripts/makeNPrankPlots.py $inver.${outver}_HH_13TeV_${outver}_Systs_lephad_SM_BDT_0 0
```

so for 300 GeV: 
```
python scripts/runNPranking.py $inver.${outver}_HH_13TeV_${outver}_Systs_lephad_SM_BDT_300 300
```

input root file directory structure should be :

1. all the nominal histograms under root directory. 

2. all systs  histograms should be under "Systematics" folder.

that's it.

To get the ranking plot first you need to make workspace with ```python Analysis_HH.py <outputversion>``` with all systs 
