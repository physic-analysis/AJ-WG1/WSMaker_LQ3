#include <set>
#include <unordered_map>
#include <utility>
#include <iostream>
#include <map>

#include <TString.h>

#include "WSMaker/configuration.hpp"
#include "WSMaker/containerhelpers.hpp"
#include "WSMaker/sample.hpp"
#include "samplesbuilder_hh.hpp"

void SamplesBuilder_HH::declareSamples() {
  TString massPoint = m_config.getValue("MassPoint", "1000");
  TString type = m_config.getValue("Type", "RSG");
  int injection = m_config.getValue("DoInjection", 0);
  TString massInjection = TString::Itoa(injection, 10);

  // data or pseudo-data should always be there
  addData();

  // signal
  if (type == "RSGc2") {
    addSample(Sample("Ghhbbtautau" + massPoint + "c20", SType::Sig, kViolet));
    addSample(Sample("RS_G_hh_bbtt_hh_c20_M" + massPoint, SType::Sig, kViolet));
  } else if (type == "RSGc1") {
    addSample(Sample("Ghhbbtautau" + massPoint + "c10", SType::Sig, kViolet));
    addSample(Sample("RS_G_hh_bbtt_hh_c10_M" + massPoint, SType::Sig, kViolet));  } else if (type == "RSG") {
    addSample(Sample("Ghhbbtautau" + massPoint + "c10", SType::Sig, kViolet));
    addSample(Sample("RS_G_hh_bbtt_hh_c10_M" + massPoint, SType::Sig, kViolet));
  } else if (type == "2HDM") {
    addSample(Sample("Hhhbbtautau"+massPoint, SType::Sig, kViolet));
    addSample(Sample("X"+massPoint, SType::Sig, kViolet));
  } else if (type.Contains("SM")) {
    if (type == "SM") {
      addSample(Sample("hhttbb", SType::Sig, kViolet));
    } else if (type == "SMRW") {
      addSample(Sample("hhttbbRW", SType::Sig, kViolet));
    } else {
      TString sample = "hhttbb" + type;
      sample.ReplaceAll("SM", "");
      addSample(Sample(sample, SType::Sig, kViolet));
    } 
  } else if (type == "ZZ"){
    addSample(Sample("ZZllqq", SType::Sig, kViolet));
  } else if (type == "LQ3") {
    addSample(Sample("LQ3Up"+massPoint, SType::Sig, kViolet));
  }


  if(injection>0 && massInjection != massPoint) {
    std::cout << "INJECTION " << massInjection << std::endl;

    if (type == "RSGc2") {
      addSample(Sample("Ghhbbtautau" + massInjection + "c20", SType::None, kViolet));
    } else if (type == "RSGc1") {
      addSample(Sample("Ghhbbtautau" + massInjection + "c10", SType::None, kViolet));
    } else if (type == "RSG") {
      addSample(Sample("Ghhbbtautau" + massInjection + "c10", SType::None, kViolet));
    }else if (type == "2HDM") {
      addSample(Sample("Hhhbbtautau"+massInjection, SType::None, kViolet));
      addSample(Sample("InjX"+massInjection, SType::None, kViolet));
    } else if (type == "SM") {
      addSample(Sample("hhttbb", SType::None, kViolet));
    } else if (type == "LQ3") {
//      addSample(Sample("LQ3btaubtau"+massInjection, SType::None, kViolet));
      addSample(Sample("LQ3Up"+massInjection, SType::None, kViolet));
    }
  }

  // TODO add spectator samples

  // SM Higgs
  addBkgs( { {"WlvH125", kRed}, {"ggZvvH125", kRed}, {"qqZvvH125", kRed},
           {"ggZllH125", kRed}, {"qqZllH125", kRed} } );

  addBkgs( { {"VH", kRed } });
  addBkgs( { {"ttH", kRed } });

  // Z->tautau + jets
  addBkgs( { {"Zttl", kAzure - 9} } );
  addBkgs( { {"Zttcl", kAzure - 4}, {"Zttcc", kAzure - 2}, {"Zttbl", kAzure + 2}, {"Zttbc", kAzure + 3}, {"Zttbb", kAzure + 4} } );

  // W->taunu + jets
  addBkgs( { {"Wtt", kGreen} } );
  addBkgs( { {"Wttl", kGreen} } );
  addBkgs( { {"Wttcl", kGreen}, {"Wttcc", kGreen}, {"Wttbl", kGreen}, {"Wttbc", kGreen}, {"Wttbb", kGreen} } );

 // Z->tautau + jets
  addBkgs( { {"ZttMGl", kAzure - 9} } );
  addBkgs( { {"ZttMGcl", kAzure - 4}, {"ZttMGcc", kAzure - 2}, {"ZttMGbl", kAzure + 2}, {"ZttMGbc", kAzure + 3}, {"ZttMGbb", kAzure + 4} } );

  // W->taunu + jets
  addBkgs( { {"WttMG", kGreen} } );
  addBkgs( { {"WttMGl", kGreen} } );
  addBkgs( { {"WttMGcl", kGreen}, {"WttMGcc", kGreen}, {"WttMGbl", kGreen}, {"WttMGbc", kGreen}, {"WttMGbb", kGreen} } );

  // Sherpa version of ee/mumu in base

  // W+jets
  addBkgs( { {"W", kGreen - 9} } );
  addBkgs( { {"WMG", kGreen - 9} } );
  addBkgs( { {"WMGl", kGreen - 9} } );
  addBkgs( { {"WMGcl", kGreen - 6}, {"WMGcc", kGreen - 3}, {"WMGbl", kGreen + 2}, {"WMGbc", kGreen + 3}, {"WMGbb", kGreen + 4} } );

  // Z+jets
  addBkgs( { {"ZMGl", kAzure - 9} } );
  addBkgs( { {"ZMGcl", kAzure - 4}, {"ZMGcc", kAzure - 2}, {"ZMGbl", kAzure + 2}, {"ZMGbc", kAzure + 3}, {"ZMGbb", kAzure + 4} } );


  // diboson
  if (type == "ZZ"){
    addBkgs( { {"WZ", kGray}, {"ZZvvqq", kGray + 1}, {"WW", kGray + 3} } );
  } else{
    addBkgs( { {"WZ", kGray}, {"ZZ", kGray + 1}, {"WW", kGray + 3} } );
  }

  // DY
  addBkgs( { {"DY", kOrange}, {"DYtt", kOrange + 1} } );

  // SM h
  addBkgs( { {"ZllH125", kOrange} } );

  // ttbar, stop, sherpa Zll & Wlnu ... 
  // W+jets
  addBkgs( { {"Wl", kGreen - 9} } );
  addBkgs( { {"Wcl", kGreen - 6}, {"Wcc", kGreen - 3}, {"Wbl", kGreen + 2}, {"Wbc", kGreen + 3}, {"Wbb", kGreen + 4} } );

  // Z+jets
  addBkgs( { {"Zl", kAzure - 9} } );
  addBkgs( { {"Zcl", kAzure - 4}, {"Zcc", kAzure - 2}, {"Zbl", kAzure + 2}, {"Zbc", kAzure + 3}, {"Zbb", kAzure + 4} } );

  // Top and single-top
  addBkgs( { {"ttbar", kOrange} } );
  addBkgs( { {"stopt", kYellow - 7}, {"stops", kYellow - 5}, {"stopWt", kOrange + 3} } );

  //addBkgs( { {"ttbarFake", kOrange - 6} } );

  addSample(Sample("ttbarTFFR", SType::DataDriven, kOrange -6));
  addSample(Sample("ttbarFTFR", SType::DataDriven, kOrange -6));
  addSample(Sample("ttbarFFFR", SType::DataDriven, kOrange -6));

  addSample(Sample("stoptTFFR", SType::DataDriven, kYellow - 7));
  addSample(Sample("stoptFTFR", SType::DataDriven, kYellow - 7));
  addSample(Sample("stoptFFFR", SType::DataDriven, kYellow - 7));

  addSample(Sample("stopsTFFR", SType::DataDriven, kYellow - 5));
  addSample(Sample("stopsFTFR", SType::DataDriven, kYellow - 5));
  addSample(Sample("stopsFFFR", SType::DataDriven, kYellow - 5));

  addSample(Sample("stopWtTFFR", SType::DataDriven, kYellow + 3));
  addSample(Sample("stopWtFTFR", SType::DataDriven, kYellow + 3));
  addSample(Sample("stopWtFFFR", SType::DataDriven, kYellow + 3));

  addSample(Sample("Fake", SType::DataDriven, kPink + 1));
  addSample(Sample("QCD", SType::DataDriven, kPink + 1));

}

void SamplesBuilder_HH::declareKeywords() {
  // use standard keywords
  m_keywords = {
    {"Diboson", {"WZ", "ZZ", "WW"}},
    {"VHVV", {"WZ", "ZZ", "WW"}},
    {"Zjets", {"Zcl", "Zcc", "Zbl", "Zbb", "Zl", "Zbc"}},
    {"Zc", {"Zcl", "Zcc"}},
    {"Zb", {"Zbl", "Zbb", "Zbc"}},
    {"Zhf", {"Zbl", "Zbb", "Zbc", "Zcc"}},
    {"ZbORc", {"Zbl", "Zbb", "Zbc", "Zcl", "Zcc"}},
    {"Wjets", {"Wcl", "Wcc", "Wbl", "Wbb", "Wl", "Wbc"}},
    {"Wb", {"Wbl", "Wbb", "Wbc"}},
    {"Whf", {"Wcc", "Wbl", "Wbb", "Wbc"}},
    {"WbbORcc", {"Wcc", "Wbb"}},
    {"WbcORbl", {"Wbl", "Wbc"}},
    {"Stop", {"stops", "stopt", "stopWt"}},
    {"Top", {"ttbar", "stops", "stopt", "stopWt"}}
  };

  for (const auto& spair : m_samples) {
    const Sample& s = spair.second;
    // Higgs samples deserve shorthands too
    if(s.name().Contains("ZvvH") && !s.name().Contains("MIL")) {
      m_keywords["ZvvH"].insert(s.name());
      m_keywords["Higgs"].insert(s.name());
      m_keywords["VHVV"].insert(s.name());
    }
    if(s.name().BeginsWith("WlvH")) {
      m_keywords["WlvH"].insert(s.name());
      m_keywords["Higgs"].insert(s.name());
      m_keywords["VHVV"].insert(s.name());
    }
    if(s.name().Contains("ZllH")) {
      m_keywords["ZllH"].insert(s.name());
      m_keywords["Higgs"].insert(s.name());
      m_keywords["VHVV"].insert(s.name());
    }
    if(s.name().BeginsWith("qq") || s.name().BeginsWith("Wlv")) {
      m_keywords["qqVH"].insert(s.name());
    }
    if(s.name().BeginsWith("qqZ")) {
      m_keywords["qqZH"].insert(s.name());
    }
    if(s.name().BeginsWith("ggZ")) {
      m_keywords["ggZH"].insert(s.name());
    }
  }



  // and add a few more
  for (const auto& spair : m_samples) {
    const Sample& s = spair.second;

    m_keywords["ttbarFake"].insert({"ttbarFTFR", "ttbarTFFR", "ttbarFFFR"});
    m_keywords["stopFake"].insert({"stopsFTFR", "stopsTFFR", "stopsFFFR", "stoptFTFR", "stoptTFFR", "stoptFFFR", "stopWtFTFR", "stopWtTFFR", "stopWtFFFR"});
    m_keywords["topFake"].insert({"ttbarFTFR", "ttbarTFFR", "ttbarFFFR", "stopsFTFR", "stopsTFFR", "stopsFFFR", "stoptFTFR", "stoptTFFR", "stoptFFFR", "stopWtFTFR", "stopWtTFFR", "stopWtFFFR"});

    // MG aliases
    m_keywords["Zl"].insert("ZMGl"); 
    m_keywords["Zcl"].insert("ZMGcl"); 
    m_keywords["Zcc"].insert("ZMGcc"); 
    m_keywords["Zbl"].insert("ZMGbl"); 
    m_keywords["Zbc"].insert("ZMGbc"); 
    m_keywords["Zbb"].insert("ZMGbb"); 

    m_keywords["Zttl"].insert("ZttMGl"); 
    m_keywords["Zttcl"].insert("ZttMGcl"); 
    m_keywords["Zttcc"].insert("ZttMGcc"); 
    m_keywords["Zttbl"].insert("ZttMGbl"); 
    m_keywords["Zttbc"].insert("ZttMGbc"); 
    m_keywords["Zttbb"].insert("ZttMGbb"); 

    m_keywords["Wl"].insert("WMGl"); 
    m_keywords["Wcl"].insert("WMGcl"); 
    m_keywords["Wcc"].insert("WMGcc"); 
    m_keywords["Wbl"].insert("WMGbl"); 
    m_keywords["Wbc"].insert("WMGbc"); 
    m_keywords["Wbb"].insert("WMGbb"); 

    m_keywords["Wttl"].insert("WttMGl"); 
    m_keywords["Wttcl"].insert("WttMGcl"); 
    m_keywords["Wttcc"].insert("WttMGcc"); 
    m_keywords["Wttbl"].insert("WttMGbl"); 
    m_keywords["Wttbc"].insert("WttMGbc"); 
    m_keywords["Wttbb"].insert("WttMGbb"); 

    // Aliases for Zee/mumu or Ztautau (e.g. to apply same XS error to both)
    m_keywords["ZlOrZttl"].insert({"Zl", "Zttl", "ZMGl", "ZttMGl"});
    m_keywords["ZclOrZttcl"].insert({"Zcl", "Zttcl", "ZMGcl", "ZttMGcl"});
    m_keywords["ZccOrZttcc"].insert({"Zcc", "Zttcc","ZMGcc", "ZttMGcc"});
    m_keywords["ZblOrZttbl"].insert({"Zbl", "Zttbl","ZMGbl", "ZttMGbl"});
    m_keywords["ZbcOrZttbc"].insert({"Zbc", "Zttbc","ZMGbc", "ZttMGbc"});
    m_keywords["ZbbOrZttbb"].insert({"Zbb", "Zttbb","ZMGbb", "ZttMGbb"});

    // Aliases for Wenu/munu or Wtaunu (e.g. to apply same XS error to both)
    m_keywords["WlOrWttl"].insert({"Wl", "Wttl", "WMGl", "WttMGl"});
    m_keywords["WclOrWttcl"].insert({"Wcl", "Wttcl", "WMGcl", "WttMGcl"});
    m_keywords["WccOrWttcc"].insert({"Wcc", "Wttcc","WMGcc", "WttMGcc"});
    m_keywords["WblOrWttbl"].insert({"Wbl", "Wttbl","WMGbl", "WttMGbl"});
    m_keywords["WbcOrWttbc"].insert({"Wbc", "Wttbc","WMGbc", "WttMGbc"});
    m_keywords["WbbOrWttbb"].insert({"Wbb", "Wttbb","WMGbb", "WttMGbb"});

    // Higgs samples deserve shorthands too
    if(s.name().Contains("hhbbtautau")) {
      m_keywords["hh"].insert(s.name());
    }
  }
}

void SamplesBuilder_HH::declareSamplesToMerge() {
  declareMerging(Sample("VH125", SType::Bkg, kRed), {"Higgs"});
  declareMerging(Sample("diboson", SType::Bkg, kGray), {"Diboson"});
  declareMerging(Sample("stop", SType::Bkg, kYellow - 7), {"Stop"});
  declareMerging(Sample("Wjets", SType::Bkg, kGreen + 4), {"Wbb", "Wbc", "Wbl", "Wcc", "Wcl", "Wl"}); //, "W", "WMG"});
  declareMerging(Sample("Wttjets", SType::Bkg, kGreen + 4), {"Wttbb", "Wttbc", "Wttbl", "Wttcc", "Wttcl", "Wttl"}); //, "Wtt", "WMGtt"});
  declareMerging(Sample("Zhf", SType::Bkg, kAzure + 4), {"Zbb", "Zbc", "Zcc"});
  declareMerging(Sample("Ztthf", SType::Bkg, kAzure + 4), {"Zttbb", "Zttbc", "Zttcc"});
  declareMerging(Sample("Zlf", SType::Bkg, kAzure + 4), {"Zl", "Zcl", "Zbl"});
  declareMerging(Sample("Zttlf", SType::Bkg, kAzure + 4), {"Zttl", "Zttcl", "Zttbl"});

  declareMerging(Sample("ttbarFake", SType::DataDriven, kOrange - 6), {"ttbarFTFR", "ttbarTFFR", "ttbarFFFR"});
  declareMerging(Sample("stopFake", SType::DataDriven, kYellow - 5), {"stopsFTFR", "stopsTFFR", "stopsFFFR", "stoptFTFR", "stoptTFFR", "stoptFFFR", "stopWtFTFR", "stopWtTFFR", "stopWtFFFR"});
}

