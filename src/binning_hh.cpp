#include "binning_hh.hpp"

#include <iostream>
#include <cmath>
#include <map>
#include <memory>

#include <TH1.h>
#include <TString.h>

#include "WSMaker/binning.hpp"
#include "WSMaker/configuration.hpp"
#include "WSMaker/category.hpp"
#include "WSMaker/properties.hpp"

#include "TransformTool/HistoTransform.h"

class Sample;

BinningTool_HH::BinningTool_HH(const Configuration& conf) :
  BinningTool(conf)
{

}

void BinningTool_HH::createInstance(const Configuration& conf) {
  if( !the_instance ) {
    std::cout << "INFO:    BinningTool_HH::createInstance() BinningTool pointer is NULL." << std::endl;
    std::cout << "         Will instanciate the BinningTool service first." << std::endl;
    the_instance.reset(new BinningTool_HH(conf));
  }
  else {
    std::cout << "WARNING: BinningTool_HH::createInstance() BinningTool pointer already exists." << std::endl;
    std::cout << "         Don't do anything !" << std::endl;
  }
}


std::vector<int> BinningTool_HH::getCategoryBinning(const Category& c) {
    std::cout << "BinningTool_HH::getCategoryBinning():" << std::endl;

  // For some stupid reason the bins need to go backwards!

  int forceRebin = m_config.getValue("ForceBinning", 0);
  bool postfit = m_config.getValue("Postfit", 0);

  if(forceRebin > 0) {
    return {forceRebin};
  }

  if (m_config.getValue("OneBin", false)) {
    return oneBin(c);
  }

  if (c[Property::nTag] == 0) {
    return oneBin(c);
  }

//   if (c(Property::dist) == "mMMC") {
//     if (postfit) 
//       return {4};    
//     else
//       return {600, 254, 216, 190, 173, 158, 144, 129, 117, 107, 96, 84, 71}; // 8%
// 
//   }

//   if (c(Property::dist) == "TauPt") {
//     return {200, 144, 69, 51, 41, 32, 27, 23, 20}; // 5%
//   }

  if (c(Property::dist).Contains("MLQ")) {
    return {2000, 178, 114, 98, 88, 79, 70, 61, 52, 43, 34, 25, 16, 7};
  }

  if (c(Property::dist) == "sT") {
    return {2000, 196, 121, 107, 97, 91, 85, 79, 73, 67, 61, 55, 49, 43, 37, 31, 25};

  }

  if (c(Property::dist) == "mLL") {
      return oneBin(c);
  }


  //std::vector<int> res{20};
  //std::vector<int> res{220, 246, 272, 298, 324, 350, 376, 402, 454, 550, 1200};

  //std::vector<int> res{0, 257, 260, 270, 280, 290, 300, 310, 320, 400, 450, 2000};

  std::vector<int> res;

  TString massPoint = m_config.getValue("MassPoint", "1000");
  TString type = m_config.getValue("Type", "RSG");

  if (c(Property::spec) == "TauHH") {
    if (c(Property::dist).Contains("mHH")) {
      if(postfit) res={5};
      //if(postfit) res={30};
      else {
	res = {1200, 400, 300, 85};
	if (massPoint == "600"|| massPoint == "700"||massPoint == "800"||massPoint == "900"||massPoint == "1000") res = {1200,  85};
      } 
    }
    else if (c(Property::dist).Contains("mMMC")) {
      if (postfit) return {4};
      else return {600, 254, 216, 190, 173, 158, 144, 129, 117, 107, 96, 84, 71}; // 8%
    } else if (c(Property::dist).Contains("METCent")) {
      res = {2};
    } else if (c(Property::dist).Contains("mBB")) {
      res = {5};
    } else if (c(Property::dist).Contains("dRBB")) {
      res = {5};
    } else if (c(Property::dist).Contains("DRTauTau")) {
      res = {5};  
    } else if (c(Property::dist).Contains("Tau0Pt")) {
      res = {5};  
    } else if (c(Property::dist).Contains("Tau1Pt")){
      res = {5};
    } //else if (type.Contains("SM")){ 
    //  res = {1002, 647};
    // }
    else {
      TH1* sig = c.getSigHist();
      TH1* bkg = c.getBkgHist(); 
      m_htrafo->trafoSixY = 10;
      m_htrafo->trafoSixZ = 5;

      m_htrafo->trafo14MinBkg=5;

      std::vector<int> bins;
      if (type.Contains("SM")) {
	bins = m_htrafo->getRebinBins(bkg, sig, 14, 0.50);
      } else {
	bins = m_htrafo->getRebinBins(bkg, sig, 14, 0.50);
      }
      

      delete sig;
      delete bkg;

      for (int i=0; i<bins.size(); i++){
	std::cout<<"Bin "<<i<<" "<<bins[i]<<std::endl;
      }

      return bins;

    }
  } else {
    if (c(Property::dist).Contains("mHH")) {
      //if (postfit) return {5};
      //else return {1200, 546, 411, 383, 361, 339, 317, 295, 273, 251}; // 12%
      res = {5};
    } else if (c(Property::dist).Contains("mMMC")) {
      //if (postfit) return {4};
      //else return {600, 254, 216, 190, 173, 158, 144, 129, 117, 107, 96, 84, 71}; // 8%
      res = {4};
    } else if (c(Property::dist).Contains("METCent")) {
      res = {2};
    } else if (c(Property::dist).Contains("MET")) {
      res = {5};
    } else if (c(Property::dist).Contains("dPhiHBB")) {
      res = {4};
    } else if (c(Property::dist).Contains("MtW")) {
      res = {4};
    } else if (c(Property::dist).Contains("mbb")) {
      res = {5};
    } else if (c(Property::dist).Contains("pTBB")) {
      res = {2};
    } else if (c(Property::dist).Contains("pTB2")) {
      res = {2};
    } else if (c(Property::dist).Contains("dPtLepTau")) {
      res = {1};
    } else if (c(Property::dist).Contains("dRbb")) {
      res = {5};
    } else if (c(Property::dist).Contains("DRTauTau")) {
      res = {5};  
    } else if (c(Property::dist).Contains("Mhh")) {
      res = {5};  
    } else if (c(Property::dist).Contains("Tau0Pt")) {
      res = {5};  
    } else if (c(Property::dist).Contains("Tau1Pt")) {
      res = {5};  
    } 
    else {
      // BDT scores
      TH1* sig = c.getSigHist();
      TH1* bkg = c.getBkgHist(); 
      m_htrafo->trafoSixY = 10;
      m_htrafo->trafoSixZ = 5;
      //std::vector<int> bins = m_htrafo->getRebinBins(bkg, sig, 13, 0.08);
      //std::vector<int> bins = m_htrafo->getRebinBins(bkg, sig, 6, 0);
      std::vector<int> bins;
      if (type.Contains("SM") || c[Property::LTT]) {
	bins = m_htrafo->getRebinBins(bkg, sig, 14, 0.40);
      } else {
	bins = m_htrafo->getRebinBins(bkg, sig, 14, 0.20);
      }

      delete sig;
      delete bkg;

      for (int i=0; i<bins.size(); i++){
	std::cout<<" Bin [" << i << "] : " <<bins[i]<<std::endl;
      }

      return bins;
      //return {10};
    }
  } 

  return res;

}

void BinningTool_HH::changeRange(TH1* h, const Category& c) {

  TString dist = c(Property::dist);

  bool postfit = m_config.getValue("Postfit", 0);


  // special case: norm-only workspaces
  if( m_config.getValue("OneBin", false)) {
    return;
  }

  if(dist == "mMMC" && !postfit ) {
    changeRangeImpl(h, 0., 600., false);
  } else if(dist == "mLL") {
    changeRangeImpl(h, 81., 101., false);
  } else if (dist == "dPhiHBB") { 
    changeRangeImpl(h, 0., 3.2, false);
  } else if (dist == "MtW") { 
    changeRangeImpl(h, 0., 200., false);
  }

  return;
}


void BinningTool_HH::changeRangeAfter(TH1* h, const Category& c) {

  TString dist = c(Property::dist);

  // special case: norm-only workspaces
  if( m_config.getValue("OneBin", false)) {
    return;
  }

//   if(dist == "BDT") {
//     changeRangeImpl(h, 0, 1., false);
//   }

  return;
}
