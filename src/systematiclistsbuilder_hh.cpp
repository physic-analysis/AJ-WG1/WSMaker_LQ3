#include "systematiclistsbuilder_hh.hpp"

#include <algorithm>
#include <map>
#include <unordered_map>
#include <vector>
#include <iostream>

#include <TString.h>

#include "WSMaker/configuration.hpp"
#include "WSMaker/containerhelpers.hpp"
#include "WSMaker/properties.hpp"
#include "WSMaker/sample.hpp"
#include "WSMaker/systematic.hpp"
#include "WSMaker/finder.hpp"

void SystematicListsBuilder_HH::fillHistoSystsRenaming() {

  // Renaming for Zll CR
  m_renameHistoSysts.emplace("SysJET_SR1_JET_GroupedNP_1", "SysJET_GroupedNP_1");
  m_renameHistoSysts.emplace("SysJET_SR1_JET_GroupedNP_2", "SysJET_GroupedNP_2");
  m_renameHistoSysts.emplace("SysJET_SR1_JET_GroupedNP_3", "SysJET_GroupedNP_3");

}

void SystematicListsBuilder_HH::listAllUserSystematics(bool useFltNorms)
{
  using P = Property;

  //normFact("All", {{"MC", "DataDriven"}}, (10.0/3.2), 0, 10, true);
  std::vector<TString> sig_decorr = m_config.getStringList("DecorrPOI");

  TString type = m_config.getValue("Type", "RSG");
  TString massPoint = m_config.getValue("MassPoint", "1000");

  // for injection at an arbitrary mass setup in the config file
  int injection     = m_config.getValue("DoInjection", 0);
  TString massInjection = TString::Itoa(injection, 10);

  //std::map<int, double> xsecs2HDM = {{300, 0.0123989}, {400, 0.00454397}, {500, 0.00346788}};  // Median limit mus (single-mass training)
  std::map<int, double> xsecs2HDM = {{300, 0.00881197}, {400, 0.00269636}, {500, 0.00188319}};  // Median limit mus (combined-mass training)
  std::map<int, double> xsecsRSG = {{300, 5.48578}, {400, 0.791713}, {500, 0.434995}};  // Median limit mus (combined-mass training)

  //  std::map<int, double> xsecs2HDM ={{300, 0.00479603}, {400, 0.000999479}, {500, 0.00115362}};// hadhad median limit mu 
  //  std::map<int, double> xsecsRSG ={{300, 2.37155}, {400, 0.248326}, {500, 0.263015}};//hadhad median limit mu 


  double xsec_injected = 1;
  if (type.Contains("RSG")) xsec_injected = xsecsRSG.find(injection) != xsecsRSG.end() ? xsecsRSG[injection] : 1;
  if (type.Contains("2HDM")) xsec_injected = xsecs2HDM.find(injection) != xsecs2HDM.end() ? xsecs2HDM[injection] : 1;
  if (type.Contains("SM")) xsec_injected = 26;
  //  if(type.Contains("SM")) xsec_injected = 17;//hadhad
      
  if(injection>0 && massInjection != massPoint) { 
   normFact("muInjection", {"None"}, xsec_injected, -10, 10, true); // this one is constant
  }

  if (type.Contains("SM") || type.Contains("ZZ") || (type.Contains("RSG") && (massPoint.Contains("260"))) || (type.Contains("LQ") && (massPoint.Contains("300") || massPoint.Contains("1400") || massPoint.Contains("1300") || massPoint.Contains("1500") || massPoint.Contains("1700") ))) {
      std::cout << "Improve the convergence : " << __FILE__ << " " << __LINE__ << std::endl;
    normFact("SigScale", {"Sig"}, 1000, 0, 2000, true);  // to improve fit convergence
    if(massInjection == massPoint){
      xsec_injected /= 1000;
    }
  }

  // Hack for buggy inputs for lephad
  //if (type.Contains("LQ") && massPoint.Contains("700")){
  //  normFact("SigScale", {"Sig"}, 0.602, 0, 2000, true);
  //}

  //if (type.Contains("SM")){
  //  normFact("SigScale", {"Sig"}, 0.612, 0, 2000, true);
  //}

  // list them all !
  if(sig_decorr.size() != 0) { // TODO or just extend decorrSysForCategories for the POIs ?
    std::vector<Property> decorrelations;
    for(auto& d : sig_decorr) {
      decorrelations.push_back(Properties::props_from_names.at(d));
    }
    addPOI("SigXsecOverSM", {"Sig", {}, std::move(decorrelations)}, 1, -40, 40);
  } else if ( injection>0 || type == "SM") {
    addPOI("SigXsecOverSM", {"Sig"}, xsec_injected, -40, 40);
  } else {
    addPOI("SigXsecOverSM", {"Sig"}, 1, -40, 40);
  }

  //return;  // HACK to allow scaling of lumi only for stat limits
  
  // Syst norm yield: get from Nicolina
  //normSys("SysTheorySig", 0.20, {"Sig"});


  // ttbar ...

  // Cross-section
  bool fltTop = ((CategoryIndex::hasMatching(P::nTag==1) || CategoryIndex::hasMatching(P::nTag==2)) 
		 && CategoryIndex::hasMatching(P::spec=="TauLH")); //not floating ttbar for hadhad alone
  //fltTop = false; // Need this for hadhad alone.  TODO: make steerable so true for lephad alone (no L0 in regtrk)
  
  bool combdecorr = CategoryIndex::hasMatching(P::spec=="TauLH") && CategoryIndex::hasMatching(P::spec=="TauHH") 
    && m_config.getValue("DecorrTTbarLHVsHH", false);//decorr hadhad ttbar in combined fit


  bool CombDecorrRatios = true;//include ratio uncertainties on ttbar for HadHad SR and ZCR relative to LH SR where derived

  if(useFltNorms && fltTop && combdecorr) {
    // hadhad SR floats within acceptance and XS
    normSys("SysTtbarAcc2Tag", 1-0.37, 1+0.34, {"ttbar", {{P::spec,"TauHH"}}});
    normSys("SysTtbarXS", 0.06, {"ttbar", {{P::spec,"TauHH"}}});
    // lephad and ZCR correlated and free to float
    normFact("ttbar", {"ttbar", {{P::spec,"TauLH"}, {P::nLep, 2}}});
  } else if(useFltNorms && fltTop && CombDecorrRatios){
    normSys("SysRatioHHSRTtbarAcc2Tag", 1-0.32, 1+0.30, {"ttbar", {{P::spec,"TauHH"}}});//HadHad SR
    normSys("SysRatioZCRTtbarAcc2Tag", 1-0.093, 1+0.081, {"ttbar", {{P::nLep, 2}}});//Zmumu CR
    normFact("ttbar", {"ttbar"});  
  } else if(useFltNorms && fltTop) {
    //normFact("ttbar", {"ttbar", {{P::spec,"TauLH"}}});
    normFact("ttbar", {"ttbar"}); 
  } else {
    sampleNormSys("ttbar", 0.06);

    // Generator acceptance variations for mMMC or mHH (norm only) . 
    /*
    if (m_config.getValue("Type", "RSG") == "SM") {
      normSys("SysTtbarAcc0Tag", 1-0.135, 1+0.084, {"ttbar", {{{{P::nTag, 0}},{{P::spec,"TauLH"}}}} });
      normSys("SysTtbarAcc1Tag", 1-0.257, 1+0.212, {"ttbar", {{{{P::nTag, 1}},{{P::spec,"TauLH"}}}} });
      normSys("SysTtbarAcc2Tag", 1-0.223, 1+0.196, {"ttbar", {{{{P::nTag, 2}},{{P::spec,"TauLH"}}}} });
      normSys("SysTtbarAcc2Tag", 1-0.223, 1+0.196,  {"ttbar", {{{{P::nTag, 2}},{{P::spec,"TauHH"}}}} }); 
    } else {
      normSys("SysTtbarAcc0Tag", 1-0.178, 1+0.093, {"ttbar", {{{{P::nTag, 0}},{{P::spec,"TauLH"}}}} });
      normSys("SysTtbarAcc1Tag", 1-0.197, 1+0.169, {"ttbar", {{{{P::nTag, 1}},{{P::spec,"TauLH"}}}} });
      normSys("SysTtbarAcc2Tag", 1-0.286, 1+0.233, {"ttbar", {{{{P::nTag, 2}},{{P::spec,"TauLH"}}}} });
      normSys("SysTtbarAcc2Tag", 1-0.345, 1+0.271, {"ttbar", {{{{P::nTag, 2}},{{P::spec,"TauHH"}}}}});//Oct 4
    }
    */
    normSys("SysTtbarAcc2Tag", 1-0.37, 1+0.34, {"ttbar", {{P::spec,"TauHH"}}});//hadhad SR
    normSys("SysTtbarAcc2Tag", 0.11, {"ttbar", { {P::nLep, 2}}});//Zmumu CR

  }

  // single top .... 

  // cross-sections from Top MC Twiki: https://twiki.cern.ch/twiki/bin/view/LHCPhysics/SingleTopRefXsec
  sampleNormSys("stops", 0.037);
  sampleNormSys("stopt", 0.042);
  sampleNormSys("stopWt", 0.054);


  // W+jets ...

  // Cross-section
  if(useFltNorms && CategoryIndex::hasMatching(P::nTag==1) && CategoryIndex::hasMatching(P::nLep==1)){ 
    normFact("W", {{"Wjets", "Wttjets", "WMG", "WttMG", "W", "Wtt"}});
  }
  else {
    normSys("SysWNorm", 0.30, {{{"Wjets", "Wttjets", "MG", "WttMG", "W", "Wtt"}}, {{P::spec,"TauLH"}}});
    normSys("SysWNorm", 0.50, {{{"Wjets", "Wttjets", "MG", "WttMG", "W", "Wtt"}}, {{P::spec,"TauHH"}}});//larger for hadhad to take into account fakes
  }

  // Z+jets ....

  bool floatHFOnlyForZ = m_config.getValue("FloatHFOnlyForZ", false);

  // Zl+Zcl

  //normFact("ZeeScale", {{"Zl", "Zcl", "Zbl"}, {{P::spec, "TauLH"}}}, 0.80, 0, 1, true);    // No SF now as 1 tag looks OK

  if(useFltNorms && !floatHFOnlyForZ && CategoryIndex::hasMatching(P::nTag==0) && 
     (CategoryIndex::hasMatching(P::nLep==0) || CategoryIndex::hasMatching(P::nLep==2))) {  
    normFact("Z", {{"Zl", "Zcl", "Zbl", "Zttl", "Zttcl", "Zttbl"}});
  }
  else {
    // From compairing 1 tag metau (well described) with 0 tag metau (0.8 SF)
    normSys("SysZNorm", 0.20, {{"Zl", "Zcl", "Zbl"}});     // Don't change the nominal as 1 tag is OK, but add difference as uncertainty
    normSys("SysZttNorm", 0.05, {{"Zttl", "Zttcl", "Zttbl"}}); 
  }

  // floating Zbb+Zbc+Zcc
  if(useFltNorms && CategoryIndex::hasMatching(P::nTag==2) && CategoryIndex::hasMatching(P::nLep==2)) { 
    normFact("Zbb", {{"ZbbOrZttbb", "ZbcOrZttbc", "ZccOrZttcc"}});

      if(CombDecorrRatios){
        normSys("SysRatioHHSRZhfAcc2Tag", 0.35, {{{"ZbbOrZttbb", "ZbcOrZttbc", "ZccOrZttcc"}}, {{P::spec,"TauHH"}}});//HadHad SR
	normSys("SysRatioLHSRZhfAcc2Tag", 0.29, {{{"ZbbOrZttbb", "ZbcOrZttbc", "ZccOrZttcc"}}, {{P::spec,"TauLH"}}});//LepHad SR
      }
  }
  else {
    //normFact("ZeeHFScale", {{"Zbb", "Zbc", "Zcc"}, {{P::spec, "TauLH"}}}, 0.80 * 1.4, 1, 2, true); // no longer applied as 1 tag is OK
    normFact("ZHFScale", {{"ZbbOrZttbb", "ZbcOrZttbc", "ZccOrZttcc"}}, 1.4, 1, 2, true); // For all Z HF (ll and tautau)
    normSys("SysZbbNorm", 0.30, {{"ZbbOrZttbb", "ZbcOrZttbc", "ZccOrZttcc"}}); // TODO: can we reduce this number?  It is ad-hoc
  }  

  // Fakes ...

  // LepHad: Conservative 21% for now using Tom's prelim ttbar FF error.  TODO: estimate
  // TODO: need to turn this off for hadhad automatically
  /////normSys("FakeMT", 0.21, {"Fake", {{P::spec, "TauLH"}}});

  // Diboson ...
  
  // Cross section from A/H->tautau and H+->taunu
  //sampleNormSys("diboson", 0.06);
    if (type == "ZZ") {
      sampleNormSys("WZ", 0.06);  
      sampleNormSys("WW", 0.06);  
      sampleNormSys("ZZvvqq", 0.06);  
      sampleNormSys("hhttbb", 0.50);        
    } else {
      sampleNormSys("Diboson", 0.06);  // Note capital Diboson here
    }


  // SM Higgs ...

  //add Higgs normalisation systematic of 28% when running resonance fits, based on 13 TeV evidence
  normSys("SysHiggsNorm", 0.28, {{"Higgs", "ZllH125", "VH"}});
  normSys("SysttHNorm", 0.30, {"ttH"});


  // multijet
  // TODO

  //normSys("ATLAS_LUMI_2015_2016", 0.032, {"MC", {{P::year, 2015}}});
  normSys("ATLAS_LUMI_2015_2016", 0.021, {"MC", {{P::year, 2015}}});

  normSys("FR_ttbarNorm", 1-0.98, 1+0.98 , {{"ttbarTFFR", "ttbarFTFR", "ttbarFFFR"}});

  normSys("SigNorm", 0.08, {"hhttbb"});
  normSys("SigNorm", 0.08, {"hhttbbRW"});

  if(type.Contains("SM")){
    normSys("SigAcc", 0.09, {"Sig", {{P::spec,"TauHH"}}});
    normSys("SigAcc", 0.05, {"Sig", {{P::spec,"TauLH"}}});
  }

  if(type.Contains("RSG") || type.Contains("2HDM")){
    normSys("SigAcc", 0.12, {"Sig"}); 
  }
}


void SystematicListsBuilder_HH::listAllHistoSystematics() {
  using T = SysConfig::Treat;
  using S = SysConfig::Smooth;
  using P = Property;
  bool yes(true);     bool no(false); // correlate years ?

  SysConfig noSmoothConfig { T::shape, S::noSmooth };
  SysConfig smoothConfig { T::shape, S::smoothRebinMonotonic};
  SysConfig skipConfig { T::skip, S::noSmooth };
  SysConfig ShapeOnlySmoothConfig {T::shapeonly, S::smoothRebinMonotonic};
  SysConfig smoothSymmConfig {T::shape, S::smoothRebinMonotonic, SysConfig::Symmetrise::symmetriseOneSided};
  SysConfig noSmoothSymmConfig { T::shape, S::noSmooth, SysConfig::Symmetrise::symmetriseOneSided};

  m_histoSysts.insert({ "SysMUONS_SCALE" , smoothConfig});
  m_histoSysts.insert({ "SysMUONS_MS" , smoothConfig});
  m_histoSysts.insert({ "SysMUONS_ID" , smoothConfig});
  m_histoSysts.insert({ "SysMUON_SCALE" , smoothConfig});
  m_histoSysts.insert({ "SysMUON_MS" , smoothConfig});
  m_histoSysts.insert({ "SysMUON_ID" , smoothConfig});

  m_histoSysts.insert({ "SysMUON_TTVA_SYS" , smoothConfig});
  m_histoSysts.insert({ "SysMUON_TTVA_STAT" , smoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_SYS" , noSmoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_STAT" , noSmoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_TrigSystUncertainty" , noSmoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_TrigStatUncertainty" , noSmoothConfig});
  m_histoSysts.insert({ "SysMUON_ISO_SYS" , smoothConfig});
  m_histoSysts.insert({ "SysMUON_ISO_STAT" , smoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_SYS_LOWPT" , smoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_STAT_LOWPT" , smoothConfig});
  m_histoSysts.insert({ "SysMUON_SAGITTA_RHO" , smoothConfig});  
  m_histoSysts.insert({ "SysMUON_SAGITTA_RESBIAS" , smoothConfig});  

  m_histoSysts.insert({ "SysEG_SCALE_ALL" , smoothConfig});
  m_histoSysts.insert({ "SysEG_RESOLUTION_ALL" , skipConfig}); // Just noise and no effect on limits but under-constrained in non-res.

//   m_histoSysts.insert({ "SysEL_EFF_Reco_TotalCorrUncertainty" , skipConfig});
//   m_histoSysts.insert({ "SysEL_EFF_ID_TotalCorrUncertainty" , skipConfig});
//   m_histoSysts.insert({ "SysEL_EFF_Iso_TotalCorrUncertainty" , skipConfig});
//   m_histoSysts.insert({ "SysEL_EFF_Trigger_TotalCorrUncertainty" , skipConfig});

  m_histoSysts.insert({ "SysEL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR" , noSmoothConfig});
  m_histoSysts.insert({ "SysEL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR" , noSmoothConfig});
  m_histoSysts.insert({ "SysEL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR" , noSmoothConfig});
  m_histoSysts.insert({ "SysEL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR" , noSmoothConfig});

  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_SME_TES_DETECTOR",  smoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_SME_TES_INSITU",   smoothConfig});
  // m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_SME_TES_MODEL", smoothConfig});   
  SysConfig noSLTConfig = { T::shape, S::smoothRebinMonotonic, SysConfig::Symmetrise::noSym, 
			    std::vector<TString>(), {{P::spec,"TauHH"}, {P::LTT, 1}}};
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_SME_TES_MODEL", noSLTConfig}); 

  SysConfig hadhadConfig = { T::shape, S::smoothRebinMonotonic, SysConfig::Symmetrise::noSym, std::vector<TString>(), {{P::spec,"TauHH"}}};
  // m_histoSysts.insert({ "SysANTITAU_BDT_CUT",  hadhadConfig});  // Replaced by CompFakes for lephad
   //m_histoSysts.insert({ "SysANTITAU_BDT_CUT",  smoothConfig});//hadhad still using it
  m_histoSysts.insert({ "SysANTITAU_BDT_CUT",  skipConfig});// Replaced by CompFakes in both channels 

  m_histoSysts.insert({ "SysMCINCREASEFF",  smoothConfig});  
  m_histoSysts.insert({ "SysDECREASEFF",  smoothConfig});  

  
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL", noSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL", noSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_JETID_TOTAL", noSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_JETID_HIGHPT", noSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_RECO_TOTAL", noSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_RECO_HIGHPT", noSmoothConfig});
  
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_TOTAL2016", skipConfig});// not recommended one
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2015", noSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_SYST2015", noSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2015", noSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016", noSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016", noSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016", noSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_TOTAL", skipConfig});// not recommended one

  m_histoSysts.insert({ "SysTAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL", smoothConfig});

  SysConfig smoothConfigSR1 = { T::shape, S::smoothRebinMonotonic, SysConfig::Symmetrise::noSym, false, {"ttbar"}};//, "stops", "stopt", "stopWt", //"Sig",
						       //"Zttbb"}}; //, "Zttcc", "Zttbc"}};


  bool np21(false);

  if (np21) {
    m_histoSysts.insert({ "SysJET_GroupedNP_1" , skipConfig});
    m_histoSysts.insert({ "SysJET_GroupedNP_2" , skipConfig});
    m_histoSysts.insert({ "SysJET_GroupedNP_3" , skipConfig});
    
    m_histoSysts.insert({ "SysJET_21NP_JET_EffectiveNP_1" , smoothConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_EffectiveNP_2" , smoothConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_EffectiveNP_3" , smoothConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_EffectiveNP_4" , smoothConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_EffectiveNP_5" , smoothConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_EffectiveNP_6" , smoothConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_EffectiveNP_7" , smoothConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_EffectiveNP_8restTerm" , smoothConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_EtaIntercalibration_Modelling" , smoothConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_EtaIntercalibration_TotalStat" , smoothConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_EtaIntercalibration_NonClosure" , smoothConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_Pileup_OffsetMu" , smoothConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_Pileup_OffsetNPV" , smoothConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_Pileup_PtTerm" , smoothConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_Pileup_RhoTopology" , smoothConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_BJES_Response" , smoothConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_PunchThrough_MC15" , smoothConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_SingleParticle_HighPt" , smoothConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_Flavor_Response" , smoothConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_Flavor_Composition" , smoothConfig}); // Should decorrelate V+jets, top, other probably
  } else {
    m_histoSysts.insert({ "SysJET_GroupedNP_1" , smoothConfig});
    m_histoSysts.insert({ "SysJET_GroupedNP_2" , smoothConfig});
    m_histoSysts.insert({ "SysJET_GroupedNP_3" , smoothConfig});
    
    m_histoSysts.insert({ "SysJET_21NP_JET_EffectiveNP_1" , skipConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_EffectiveNP_2" , skipConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_EffectiveNP_3" , skipConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_EffectiveNP_4" , skipConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_EffectiveNP_5" , skipConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_EffectiveNP_6" , skipConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_EffectiveNP_7" , skipConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_EffectiveNP_8restTerm" , skipConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_EtaIntercalibration_Modelling" , skipConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_EtaIntercalibration_TotalStat" , skipConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_EtaIntercalibration_NonClosure" , skipConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_Pileup_OffsetMu" , skipConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_Pileup_OffsetNPV" , skipConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_Pileup_PtTerm" , skipConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_Pileup_RhoTopology" , skipConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_BJES_Response" , skipConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_PunchThrough_MC15" , skipConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_SingleParticle_HighPt" , skipConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_Flavor_Response" , skipConfig});
    m_histoSysts.insert({ "SysJET_21NP_JET_Flavor_Composition" , skipConfig}); // Should decorrelate V+jets, top, other probably

  }

//   // These should go after renaming or be correlated with above
//   m_histoSysts.insert({ "SysJET_SR1_JET_GroupedNP_1" , smoothConfig});
//   m_histoSysts.insert({ "SysJET_SR1_JET_GroupedNP_2" , smoothConfig});
//   m_histoSysts.insert({ "SysJET_SR1_JET_GroupedNP_3" , smoothConfig});

  m_histoSysts.insert({ "SysJET_JER_SINGLE_NP" , smoothSymmConfig});
//  m_histoSysts.insert({ "SysJET_JER_SINGLE_NP" , SysConfig{T::shape, S::noSmooth, false, {"Bkg"}}});
//  m_histoSysts.insert({ "SysJET_JER_SINGLE_NP" , SysConfig{T::skip, S::smooth, {"Sig"}}});



  m_histoSysts.insert({ "SysJET_JvtEfficiency" , smoothConfig});

  m_histoSysts.insert({ "SysFT_EFF_extrapolation" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_extrapolation_from_charm" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_Light_0" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_Light_1" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_Light_2" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_Light_3" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_Light_4" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_B_0" , smoothConfig}); // Seems to make things more stable
  m_histoSysts.insert({ "SysFT_EFF_Eigen_B_1" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_B_2" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_C_0" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_C_1" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_C_2" , noSmoothConfig});
  //m_histoSysts.insert({ "SysFT_EFF_Eigen_C_3" , noSmoothConfig}); //Not applied for new CDI
  m_histoSysts.insert({ "SysFT_EFF_Eigen_C_3" , skipConfig}); //Not applied for new CDI

  m_histoSysts.insert({ "SysFT_EFF_extrapolation_AntiKt4EMTopoJets" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_extrapolation_from_charm_AntiKt4EMTopoJets" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_Light_0_AntiKt4EMTopoJets" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_Light_1_AntiKt4EMTopoJets" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_Light_2_AntiKt4EMTopoJets" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_Light_3_AntiKt4EMTopoJets" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_Light_4_AntiKt4EMTopoJets" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_B_0_AntiKt4EMTopoJets" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_B_1_AntiKt4EMTopoJets" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_B_2_AntiKt4EMTopoJets" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_C_0_AntiKt4EMTopoJets" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_C_1_AntiKt4EMTopoJets" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_C_2_AntiKt4EMTopoJets" , noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_C_3_AntiKt4EMTopoJets" , noSmoothConfig});


  m_histoSysts.insert({ "SysMET_SoftTrk_Scale" , smoothConfig});
  //m_histoSysts.insert({ "SysMET_JetTrk_Scale" , smoothConfig});
  m_histoSysts.insert({ "SysMET_SoftTrk_ResoPerp" , smoothSymmConfig});
  m_histoSysts.insert({ "SysMET_SoftTrk_ResoPara" , smoothSymmConfig});
  m_histoSysts.insert({ "SysPRW_DATASF" , smoothConfig}); 

  m_histoSysts.insert({ "SysTTBAR_NNLO", noSmoothSymmConfig});

  // LepHad+HadHad FF: QCD decorrelated
  SysConfig decorrConfig = { T::shape, S::smoothRebinMonotonic, SysConfig::Symmetrise::noSym, std::vector<TString>(), {{}}, {{P::spec}}};
  m_histoSysts.insert({ "SysFFStatQCD", decorrConfig});
  m_histoSysts.insert({ "SysSubtraction_bkg", decorrConfig});
   m_histoSysts.insert({ "SysCompFakes",  decorrConfig});

  // LepHad FF
   m_histoSysts.insert({ "SysSS", smoothSymmConfig});
  m_histoSysts.insert({ "SysFFStatTtbar", smoothConfig});
  m_histoSysts.insert({ "SysFF_MTW", smoothSymmConfig});
  m_histoSysts.insert({ "SysCPVarFakes", smoothConfig});
  m_histoSysts.insert({ "SysTTbarGenFakes", smoothConfig});

  // Data-driven rQCD -> not currently used
  m_histoSysts.insert({ "SysFFStatrQCD", smoothConfig});
  m_histoSysts.insert({ "SysffMC40", skipConfig}); // Too large
  m_histoSysts.insert({ "SysMC40", smoothConfig});
  m_histoSysts.insert({ "Sysff", smoothConfig});
  m_histoSysts.insert({ "SysMT40GeV", smoothConfig});


  // HadHad FF -> old
  m_histoSysts.insert({ "SysSubtractbkg_FFQCD", smoothConfig});
  m_histoSysts.insert({ "SysFFQCD_0to2tag", smoothConfig});

  //HadHad QCD FF
  m_histoSysts.insert({ "Sys1tag2tagTF", smoothConfig});
  m_histoSysts.insert({ "SysOSSS", smoothConfig});

  //HadHad top FR
   m_histoSysts.insert({ "SysFR_MTW_CUT", smoothSymmConfig});
   m_histoSysts.insert({ "SysFR_Stat", smoothConfig});
   m_histoSysts.insert({ "SysFR_STTfraction", smoothSymmConfig});
   m_histoSysts.insert({ "SysFR_CPVar", ShapeOnlySmoothConfig});  // Correlated with lephad?
   m_histoSysts.insert({ "SysFR_ttbarGen", skipConfig}); // Correlated with lephad?

   SysConfig decorrNoSmoothConfig = { T::shape, S::smoothRebinMonotonic, 
				      SysConfig::Symmetrise::noSym, std::vector<TString>(), {{}}, {{P::spec}}};

  m_histoSysts.insert({ "SysTTbarMBB", noSmoothConfig});
  m_histoSysts.insert({ "SysTTbarST" , noSmoothConfig});
  m_histoSysts.insert({ "SysTTbarPTH", noSmoothConfig});
  m_histoSysts.insert({ "SysZtautauMBB", noSmoothConfig});
  m_histoSysts.insert({ "SysZtautauPTH", noSmoothConfig});

  // TwoLepton CR
  m_histoSysts.insert({ "SysStopWtMBB", noSmoothConfig});
  m_histoSysts.insert({ "SysStopWtPTV", noSmoothConfig});
  m_histoSysts.insert({ "SysStoptMBB", noSmoothConfig});
  m_histoSysts.insert({ "SysStoptPTV", noSmoothConfig});
  m_histoSysts.insert({ "SysTTbarPTV", noSmoothConfig});
  //m_histoSysts.insert({ "SysTTbarMBB", noSmoothConfig});
  m_histoSysts.insert({ "SysMETTrigStat", skipConfig});
  m_histoSysts.insert({ "SysMETTrigTop", skipConfig});
  
  m_histoSysts.insert({ "SysZtautauPTTau", skipConfig});
  m_histoSysts.insert({ "SysZtautauST", skipConfig});
  

  // No Z modelling?

  /////////////////////////////////////////////////////////////////////////////////////////
  //
  //                          Tweaks, if needed
  //
  /////////////////////////////////////////////////////////////////////////////////////////

  // later can add some switches, e.g looping through m_histom_histoSysts.emplace( "Systs and
  // putting to T::skip all the JES NP



}






