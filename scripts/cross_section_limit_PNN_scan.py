
import sys
import ROOT as R
from array import array
from collections import OrderedDict as odict
import LQ3AtlasStyle as astyle
import json
import os
import pandas as pd
import numpy as np
import LimitPlotter
    
if __name__ == '__main__':

    R.gROOT.SetBatch(True)
    plotter = LimitPlotter.LimitPlotter('pp13_stopsbottom_NNLO+NNLL.json')

    # ATLAS style 
    astyle.SetAtlasStyle()

    name_bdt        = "LQ3LH_v1.output_LQ3_13TeV_output_StatOnly_lephad_LQ3_BDT_{}"
    # plotter.PlotExpectedCurveErrorBand( name_bdt , R.kMagenta, legend="Exp 139 fb^{-1} (BDT)", beta1_weight = True)

    # Theory curve
    plotter.PlotTheoryCurve(draw_error_band = True)

    # Expected curve
    name_prefix        = 'LQ3LH_v5.output_LQ3_13TeV_output_StatOnly_lephad_PNNScore_'
    default_parameters = 'epochs100_batchsize64_rate0p1_decay1ep05_layer32p32p32_SR_LQ3_PNN'

    for epochs in (10, 100, 500, 1000):
      for batch_size in (10, 64, 100):
        for learning_rate in ("0p01", "0p1", "0p5", "0p9"):
          for learning_rate_decay in ("1ep05",):
            for layer_size in ("32p32", "32p32p32", "32p32p32p32", "100p100p100"):
                parameters         = 'epochs{}_batchsize{}_rate{}_decay{}_layer{}_SR_LQ3_PNN'.format(epochs, batch_size, learning_rate, learning_rate_decay, layer_size)

		if name_prefix + parameters == default_parameters : # defalt config
                    continue
		else:
                    color = R.kMagenta
	        name_bdt += "{}"
	
    		plotter.PlotExpectedCurve(name_prefix + parameters + '_{}' , color, legend="Exp 139 fb^{-1} (BDT)", beta1_weight = True, scan = True)

    # Default configuration PNN
    plotter.PlotExpectedCurve( name_prefix + default_parameters + '_{}' , color=R.kBlack, legend="Exp 139 fb^{-1} (BDT)", beta1_weight = True, scan = True)
   
    best_limit = plotter.GetBestLimit() 
    best_limit_sorted = sorted(best_limit.items(), key=lambda x:x[1])

    
    array = []
    for name, value in best_limit_sorted:
        name_splitted = name.split("_")
        epoch = name_splitted[8] .replace("epochs", "")
        batch = name_splitted[9] .replace("batchsize", "")
        rate  = name_splitted[10].replace("rate", "").replace("0p","0.")
        decay = name_splitted[11].replace("decay", "").replace("ep", "e-")
        layer = name_splitted[12].replace("layer", "").replace("p","-")

        array.append([epoch, batch, rate, decay, layer, value])
    
    dataframe = pd.DataFrame(array, columns=('epoch', 'batch', 'rate', 'decay', 'layer', 'XSec'))
    dataframe.to_csv("ranking.csv")
   
    astyle.ATLASLabel(0.42,0.85,'Simulation Work in progress', plotter.GetCanvas())
    plotter.SaveCanvas()

    sys.exit()
