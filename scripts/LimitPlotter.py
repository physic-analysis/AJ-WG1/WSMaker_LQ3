
import sys
import ROOT as R
from array import array
from collections import OrderedDict as odict
import LQ3AtlasStyle as astyle
import json
import os
import pandas as pd
import numpy as np

class LimitPlotter():
    def __init__(self, filename):
        self.json_file = filename
        self.masses = [300,400,500,600,700,800,900,950,1000,1050,1100,1150,1200,1250,1350,1400,1450,1500,1550]
        self.second_hist = False

        self.isFirstLoad = True
        self.isFirstHist = True

        # Initialize TCanvas
        self.CanvasMaker()

	self.best_limit = {}

    def Mass(self,):
        return self.masses

    def GetBestLimit(self):
	return self.best_limit

    def CanvasMaker(self):
        self.canvas = R.TCanvas('c','c', 700,500)
        self.canvas.SetTopMargin(0.05)
        self.canvas.SetRightMargin(0.05)
        self.canvas.SetBottomMargin(0.16)
        self.canvas.SetLeftMargin(0.15)
        self.canvas._hists = []
        R.gPad.SetLogy(1)
        return

    def GetCanvas(self):
        return self.canvas

    def SaveCanvas(self,):
        self.canvas.SaveAs("LQ3_limit.pdf")

    def SetRange(self, graph):
        graph.SetMinimum(1e-4)
        graph.SetMaximum(10e-2)
        graph.GetXaxis().SetLimits(300, 1600)

    def branching_ratio(self,mLQ, beta):
        mbtm = 4.18
        mtop = 172.9
        mtau = 1.77686
        lam  = 0.3
        
        decay_width = {}
        decay_width[0] = ( ( mLQ**2 - mtop**2 )**2 * 3 * lam**2 * ( 1 - beta ) ) / ( 48 * R.TMath.Pi() * mLQ**3 )
        decay_width[1] = ( ( ( mLQ**2 - mbtm**2 - mtau**2 ) * 3 * lam**2 * beta  ) / ( 48 * R.TMath.Pi() * mLQ**3 ) ) * R.TMath.Sqrt( mLQ**4 + mbtm**4 + mtau**4 - 2 * ( mLQ**2 * mbtm**2 + mLQ**2 * mtau**2 + mtau**2 * mbtm**2 ))  
        return decay_width[1]/sum(decay_width.values())


    def cross_section(self):
    
        cross_section = {}
        
        data = json.load(open(os.path.join("extPackages/xsec/json", self.json_file)))
        df   = pd.DataFrame.from_dict(data["data"], orient = "index")
        # restore mass as column and sort 
        df["mass_GeV"] = df.index.astype(int)
        df = df.sort_values("mass_GeV")
        df.reset_index(inplace = True, drop = True)

        return df

    def LoadLimitCurves(self, name, color, legend="exp (LQ)", br = None, rw = False, beta1_weight = False):
        observed_limit = array('f', [])
        expected_limit = array('f', [])
        error_p1sigma = array('f', []) # plus  1 sigma
        error_p2sigma = array('f', []) # plus  2 sigma
        error_m2sigma = array('f', []) # minus 1 sigma
        error_m1sigma = array('f', []) # minus 2 sigma
    
        stat_only = "StatOnly" in name
        
        cross_section = {}
        df = self.cross_section()
        for error, xsec, mass in df.values.tolist():
            cross_section[mass] = xsec
    
        # beta = 0.5
        if not beta1_weight:
          for mass in self.Mass():
            cross_section[mass] = cross_section[mass] * self.branching_ratio(mass, 0.5)**2
    
        for mass in self.Mass():
            file_name = "output/" + name.format(mass) + "/root-files/obs/" + str(mass) + ".root"
            input_file = R.TFile.Open(file_name)
            
            limit_xs_obs = 0 
            limit_xs_exp = 0
            m1sigma = 0
            m2sigma = 0
            p1sigma = 0
            p2sigma = 0
    
            # Check the validity of the input file
            if input_file : 
                h_limit = input_file.Get("limit")
    
                if h_limit:
                    # @@@@@@@@@@@@@@@@
                    #  Blind Analysis
                    # @@@@@@@@@@@@@@@@
                    #  If you set some calculated value to the vo variable, this script show you the observed limit.
                    #  Pleeeease be careful if you delete the comment out of this vo variable.
                    # obs/exp/+2sigma/+1sigma/-1sigma/-2sigma
                    # @@@@@@@@@@@@@@@@
                    limit_xs_obs  = 0.0 #h_limit.GetBinContent(1) * cross_section[mass] 
                    limit_xs_exp  = h_limit.GetBinContent(2)      * cross_section[mass] 
                    p1sigma       = h_limit.GetBinContent(4)      * cross_section[mass] - limit_xs_exp
                    p2sigma       = h_limit.GetBinContent(3)      * cross_section[mass] - limit_xs_exp
                    m1sigma       = limit_xs_exp                  - h_limit.GetBinContent(5) * cross_section[mass]
                    m2sigma       = limit_xs_exp                  - h_limit.GetBinContent(6) * cross_section[mass]
                else:
                    print('@@@@@@@ There is no limit histograms in ' + file_name )
                
                input_file.Close()
    
            else :
                print ("@@@@@@@@@@ TFile opened fail : " + file_name )
                h_limit = None
                
            observed_limit.append(limit_xs_obs)
            expected_limit.append(limit_xs_exp)
            error_p1sigma.append(p1sigma)
            error_p2sigma.append(p2sigma)
            error_m1sigma.append(m1sigma)
            error_m2sigma.append(m2sigma)
    
        print "-"*100
        print('   ' + legend)
        print "-"*100        
        print self.Mass()
        print expected_limit

	# 13
	self.best_limit[name] = expected_limit[10]
        
        zero = array('f', [0]*len(self.Mass()))
        mass = array('f', self.Mass())
    
        self.g_expected = R.TGraph(len(self.Mass()), mass, expected_limit)
        self.g_observed = R.TGraph(len(self.Mass()), mass, observed_limit)
        self.g_1sigma   = R.TGraphAsymmErrors(len(self.Mass()), mass, expected_limit, zero, zero, error_m1sigma, error_p1sigma)
        self.g_2sigma   = R.TGraphAsymmErrors(len(self.Mass()), mass, expected_limit, zero, zero, error_m2sigma, error_p2sigma)

        self.isFirstLoad = False

    def PlotExpectedCurveErrorBand( self, name, color, legend="exp (LQ)", br = None, rw = False, beta1_weight = False):
        
        if self.isFirstLoad : 
            self.LoadLimitCurves( name = name, color=color, legend=legend, br = br, rw = rw, beta1_weight = beta1_weight) 
       
       ## Error bands
        self.g_1sigma.SetFillColor(R.kGreen)
        self.g_2sigma.SetFillColor(R.kYellow)

        self.SetRange(self.g_2sigma)
        self.g_2sigma.Draw("{}".format( "A4" if self.isFirstHist else "same 4"))
        self.g_1sigma.Draw("same 4")
        
        self.canvas._hists.append(self.g_1sigma)
        self.canvas._hists.append(self.g_2sigma)
        
        self.isFirstHist = False

        return

    def PlotTheoryCurve(self, draw_error_band = True):

        # Load cross section 
        df = self.cross_section()

        array_mass   = array('f', [])
        array_errorX = array('f', [])
        array_error  = array('f', [])
        array_xsec   = array('f', [])
        for error, xsec, mass in df.values.tolist():
            array_mass .append(mass)
            array_errorX.append(0)
            array_error.append(error)
            array_xsec .append(xsec)
        
        g_theory       = R.TGraph(len(array_mass), array_mass, array_xsec)
        g_theory_error = R.TGraphErrors(len(array_mass), array_mass, array_xsec, array_errorX, array_error)

        g_theory.SetLineColor(R.kBlack)
        g_theory.SetMarkerColor(R.kBlack)   
        g_theory_error.SetFillColor(R.kAzure-4)
    
        self.SetRange(g_theory_error)
        if draw_error_band : 
            g_theory_error.SetMinimum(1e-4)
            g_theory_error.Draw("{}".format("A3" if self.isFirstHist else "same 3"))
            g_theory.Draw("same")
        else : 
            g_theory.Draw("{}".format("AL" if self.isFirstHist else "same"))
        
        self.canvas._hists.append(g_theory)
        self.canvas._hists.append(g_theory_error)

        self.isFirstHist = False


    def PlotExpectedCurve(self, name, color, linestyle=1, legend="exp (LQ)", br = None, rw = False, beta1_weight = False, scan = False):
       
        if self.isFirstLoad : 
            self.LoadLimitCurves( name = name, color=color, legend=legend, br = br, rw = rw, beta1_weight = beta1_weight)
        if scan : 
            self.LoadLimitCurves( name = name, color=color, legend=legend, br = br, rw = rw, beta1_weight = beta1_weight)

        # Set line & marker color
        self.g_expected.SetLineColor(color)
        self.g_expected.SetMarkerColor(color)
        self.g_expected.SetLineStyle(linestyle)
       
        self.SetRange(self.g_expected)
        self.g_expected.Draw("{}".format( "AL" if self.isFirstHist else "same L"))
            
        self.canvas._hists.append(self.g_expected)
        self.second_hist = True
        
        self.isFirstHist = False

        return
