import ROOT as R
import sys, os
from argparse import ArgumentParser

def copy(f_input, f_output):
    """
    Copy root file one dir deep but only if subdir is `Systematics`
    """

    for top_directory in f_input.GetListOfKeys():
        print("*** {}".format( top_directory.GetName()) )

        if not top_directory.IsFolder(): continue
        if top_directory.GetName() in to_move:
            print(" >>> Process the {0}".format(top_directory.GetName()))

            din = f_input.Get(top_directory.GetName())
            f_output.cd()
            
            for hist in din.GetListOfKeys():
                if skip_ss and 'SS' in hist.GetName() : continue                   # Same-sign 
                if "0tag" in hist.GetName() or "3ptag" in hist.GetName(): continue # 0 or 3 tag won't be used

                tmpobj = hist.ReadObj()
                tmpobj.Write(tmpobj.GetName().replace('ptv_', 'ptv_{}_'.format(top_directory.GetName())) )

            f_input.cd()
            f_output.cd()
            
            f_output.Flush()
            
            # Add systs to top-level syst dir
            print(">>>>>>>>>>>>>>>>>>>>> " +  top_directory.GetName() + "/Systematics")
            sys_directory = f_input.Get(top_directory.GetName() + "/Systematics")
            if sys_directory:
                dout = f_output.Get("Systematics")
                if not dout:
                    dout = f_output.mkdir("Systematics")
                dout.cd()

                for hist_systematics in sys_directory.GetListOfKeys():
                    if not to_keep or '_'.join(hist_systematics.GetName().split('_')[3:]) in to_keep or top_directory.GetName() in to_keep or 'BDT' in hist_systematics.GetName() or 'PNN' in hist_systematics.GetName() or 'TauPT' in hist_systematics.GetName():
                        if skip_ss and 'SS' in hist_systematics.GetName(): continue
                        if "0tag" in hist_systematics.GetName() or "3ptag" in hist_systematics.GetName(): continue

                        print(">>> Writing {}/Systematics/{}".format(top_directory.GetName(), hist_systematics.GetName()))
                        
                        tmpobj = hist_systematics.ReadObj()
                        
                        # Add a region back in 
                        #if len(tmpobj.GetName().replace("_SS", "").replace("RSG_BDT_", "").replace("RSGc1_BDT_", "").replace("RSGc2_BDT_", "").replace("2HDM_BDT_", "").replace("LQ3_BDT", "").replace("_BDT", "").split('_Sys')[0].split('_')) >= 5:
                        #    tmpobj.Write(tmpobj.GetName())
                        #else:
                        print "  - Rename", tmpobj.GetName().replace('ptv_', 'ptv_%s_' % top_directory.GetName())
                        tmpobj.Write(tmpobj.GetName().replace('ptv_', 'ptv_%s_' % top_directory.GetName()))
                    
                f_input.cd()
                f_output.cd()
                f_output.Flush()

if __name__ == "__main__" :

    usage = 'Usage : python select_slim.py -i <input_file> -k <keep_dir> -m <move_dir>'
    argparser = ArgumentParser(usage=usage)
    argparser.add_argument('-i', '--input' , type=str, dest='input_file')
    argparser.add_argument('-o', '--output', type=str, dest='output_file')
    argparser.add_argument('-k', '--keep'  , type=str, dest='to_keep', default=['BDTScores'])
    argparser.add_argument('-m', '--move'  , type=str, dest='to_move', default=['BDTScores'])
    argparser.add_argument('-s', '--ss'    , type=int, dest='skip_ss', default=1)
    args = argparser.parse_args()
    
    input_file  = args.input_file
    output_file = args.output_file
    to_keep     = args.to_keep
    to_move     = args.to_move.split(',')
    skip_ss     = args.skip_ss

    if output_file == None:
        output_file = '13TeV_TauLH_' + input_file.replace('.root', '.slimed.root')
    
    print "Input file name : " + input_file + "/ Output file name : " +  output_file

    R.TH1.AddDirectory(False) 
    # Root file open
    f_input  = R.TFile.Open(input_file)
    f_output = R.TFile.Open("slimed_files/{}".format(output_file), "RECREATE")    
    
    copy(f_input, f_output)
    
    f_input .Close()
    f_output.Close()
