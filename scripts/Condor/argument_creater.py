
for epochs in (10, 100, 500, 1000):
  for batch_size in (10, 64, 100):
    for learning_rate in ("0p01", "0p1", "0p5", "0p9"):
      for learning_rate_decay in ("1ep05",):
        for layer_size in ("32p32", "32p32p32", "32p32p32p32", "100p100p100"):
            file_suffix = 'PNNScore_epochs{}_batchsize{}_rate{}_decay{}_layer{}'.format(epochs, batch_size, learning_rate, learning_rate_decay, layer_size)
            print("arguments = -i LQ3LH_v5 -o output --use_pnn --do_limits --region {}".format(file_suffix))
            print("queue")

