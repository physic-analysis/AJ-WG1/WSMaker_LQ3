
## setupATLAS
#source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh " "

export ANALYSISTYPE="HHbbtautau"
export ANALYSISDIR=$(pwd)
export IS_BLINDED=0

echo "Setting ANALYSISTYPE to : "$ANALYSISTYPE
source WSMakerCore/setup.sh $ANALYSISDIR/WSMakerCore $IS_BLINDED
export NCORE=4

python /afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/WSMaker_LQ3/scripts/Analysis_LQ3_Condor.py $@

