
RooNLLVar* createNLL(RooConfig* mc, RooDataSet* _data)
RooDataSet* makeAsimovData(bool doConditional, RooNLLVar* conditioning_nll, double mu_val, string* mu_str = NULL, string* mu_prof_str = NULL, double mu_val_profile = -999, bool doFit = true, double mu_injection = -1);

void Asymptotic()
{
    std::string folder = "testoutput";
    double CL = 0.95;

    RooMsgService::instance().setGlobalKillBelow(RooFit::FATAL); // In case you want to suppress RooFit warnings further, set to 1
    ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
    ROOT::Math::MinimizerOptions::SetDefaultStrategy(1); // Minimization strategy. 0-2. 0 = fastest, least robust. 2 = slowest, most robust
    ROOT::Math::MinimizerOptions::SetDefaultPrintLevel(1);  // Minuit print level

    //check inputs
    TFile f("/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/WSMaker_LQ3/output/LQ3LH_v1.output_LQ3_13TeV_output_StatOnly_lephad_LQ3_BDT_300/workspaces/combined/300.root");
    w = (RooWorkspace*)f.Get("combined");
    if (!w) {
        std::cout << "ERROR::Workspace: doesn't exist!" << std::endl;
        return;
    }

    RooConfig* mc = (ModelConfig*)w->obj("ModelConfig");
    if (!mc) {
        std::cout << "ERROR::ModelConfig doesn't exist!" << std::endl;
        return;
    }
    firstPOI = (RooRealVar*)mc->GetParametersOfInterest()->first();

    dataset = (RooDataSet*)w->data("obsData");
    if (!dataset) {
        std::cout << "ERROR::Dataset: doesn't exist!" << std::endl;
        return;
    }

    // Find injected mu before anything moves the PoI (for second case)
    double mu_inj = 1.;
    if(doInj) {     
        if ( w->var("ATLAS_norm_muInjection") ) {
            mu_inj = w->var("ATLAS_norm_muInjection")->getVal();
        } else {
            mu_inj = w->var("SigXsecOverSM")->getVal(); 
        }
    }

    obs_nll = createNLL( mc,dataset);
    map_snapshots[obs_nll] = "nominalGlobs";
    map_data_nll[dataset] = obs_nll;
    w->saveSnapshot("nominalGlobs",*mc->GetGlobalObservables());
    w->saveSnapshot("nominalNuis",( mc->GetNuisanceParameters() ? *mc->GetNuisanceParameters() : RooArgSet()));

    global_status=0;
    RooDataSet* asimovData_0 = makeAsimovData(conditionalExpected, obs_nll, 0);
    int asimov0_status       = global_status;
    
    asimov_0_nll                = createNLL( mc,asimovData_0);
    map_snapshots[asimov_0_nll] = "conditionalGlobs_0";
    map_data_nll[asimovData_0]  = asimov_0_nll;
    setMu(0);
    map_muhat[asimov_0_nll] = 0;
    saveSnapshot(asimov_0_nll, 0);
    w->loadSnapshot("conditionalNuis_0");
    w->loadSnapshot("conditionalGlobs_0");
    map_nll_muhat[asimov_0_nll] = asimov_0_nll->getVal();

    target_CLs = 1-CL;

    /* Compute expected limit */
    double med_limit = getLimit(asimov_0_nll, 0.2) // Compute expected limit

    int med_status = global_status;

    double inj_limit = 0;
    int inj_status=0;

    if(doInj) {

        std::cout << "Injected Mu " << mu_inj << std::endl;

        RooDataSet* asimovData_inj = makeAsimovData(conditionalExpected, obs_nll, 0, NULL, NULL, -999, true, mu_inj);

        int asimovinj_status=global_status;

        RooNLLVar* asimov_inj_nll = createNLL( mc,asimovData_inj);
        map_snapshots[asimov_inj_nll] = "conditionalGlobs_0";
        map_data_nll[asimovData_inj] = asimov_inj_nll;
        setMu(0);
        w->loadSnapshot("conditionalNuis_0");
        w->loadSnapshot("conditionalGlobs_0");

        target_CLs=1-CL;

        inj_limit = getLimit(asimov_inj_nll, med_limit);
        inj_status=global_status;
    }

    double sigma = med_limit/sqrt(3.84); // pretty close
    double mu_up_p2_approx = sigma*(ROOT::Math::gaussian_quantile(1 - target_CLs*ROOT::Math::gaussian_cdf( 2), 1) + 2);
    double mu_up_p1_approx = sigma*(ROOT::Math::gaussian_quantile(1 - target_CLs*ROOT::Math::gaussian_cdf( 1), 1) + 1);
    double mu_up_n1_approx = sigma*(ROOT::Math::gaussian_quantile(1 - target_CLs*ROOT::Math::gaussian_cdf(-1), 1) - 1);
    double mu_up_n2_approx = sigma*(ROOT::Math::gaussian_quantile(1 - target_CLs*ROOT::Math::gaussian_cdf(-2), 1) - 2);

    double mu_up_p2 = mu_up_p2_approx;
    double mu_up_p1 = mu_up_p1_approx;
    double mu_up_n1 = mu_up_n1_approx;
    double mu_up_n2 = mu_up_n2_approx;

    firstPOI->setRange(-5*sigma, 5*sigma);
    std::map<int, int> N_status;
    if ( betterBands ) {
        // no better time than now to do this
        // find quantiles, starting with +2, since we should be at +1.96 right now

        double init_targetCLs = target_CLs;
        firstPOI->setRange(-5*sigma, 5*sigma);
        for (int N=2;N>=-2;N--) {
            if (N < 0 && !betterNegativeBands) continue;
            if (N == 0) continue;
            target_CLs=2*(1-ROOT::Math::gaussian_cdf(fabs(N))); // change this so findCrossing looks for sqrt(qmu95)=2
            if (N < 0) direction = -1;

            //get the acual value
            double NtimesSigma = getLimit(asimov_0_nll, N*med_limit/sqrt(3.84)); // use N * sigma(0) as an initial guess
            N_status[N] += global_status;
            sigma = NtimesSigma/N;
            cout << endl;
            cout << "Found N * sigma = " << N << " * " << sigma << endl;

            string muStr,muStrPr;
            w->loadSnapshot("conditionalGlobs_0");
            double pr_val = NtimesSigma;
            if (N < 0 && profileNegativeAtZero) pr_val = 0;
            RooDataSet* asimovData_N = makeAsimovData(1, asimov_0_nll, NtimesSigma, &muStr, &muStrPr, pr_val, 0);
            //RooDataSet* asimovData_N = makeAsimovData2(asimov_0_nll, NtimesSigma, pr_val, &muStr, &muStrPr);


            RooNLLVar* asimov_N_nll = createNLL( mc,asimovData_N);
            map_data_nll[asimovData_N] = asimov_N_nll;
            map_snapshots[asimov_N_nll] = "conditionalGlobs"+muStrPr;
            w->loadSnapshot(map_snapshots[asimov_N_nll].c_str());
            w->loadSnapshot(("conditionalNuis"+muStrPr).c_str());
            setMu(NtimesSigma);

            double nll_val = asimov_N_nll->getVal();
            saveSnapshot(asimov_N_nll, NtimesSigma);
            map_muhat[asimov_N_nll] = NtimesSigma;
            if (N < 0 && doTilde)
            {
                setMu(0);
                firstPOI->setConstant(1);
                nll_val = getNLL(asimov_N_nll);
            }
            map_nll_muhat[asimov_N_nll] = nll_val;

            target_CLs = init_targetCLs;
            direction=1;
            double initial_guess = findCrossing(NtimesSigma/N, NtimesSigma/N, NtimesSigma);
            double limit = getLimit(asimov_N_nll, initial_guess);
            N_status[N] += global_status;

            if (N == 2) mu_up_p2 = limit;
            else if (N == 1) mu_up_p1 = limit;
            else if (N ==-1) mu_up_n1 = limit;
            else if (N ==-2) mu_up_n2 = limit;
        }
        direction = 1;
        target_CLs = init_targetCLs;
    }

    w->loadSnapshot("conditionalNuis_0");
    double obs_limit = doObs ? getLimit(obs_nll, med_limit) : 0;
    int obs_status=global_status;

    bool hasFailures = false;
    if (obs_status != 0 || med_status != 0 || asimov0_status != 0 || inj_status != 0) hasFailures = true;
    for (std::map<int, int>::iterator itr=N_status.begin();itr!=N_status.end();itr++) {
        if (itr->second != 0) hasFailures = true;
    }
    if (hasFailures) {
        std::cout << "--------------------------------" << std::endl;
        std::cout << "Unresolved fit failures detected" << std::endl;
        std::cout << "Asimov0:  " << asimov0_status << std::endl;
        for (std::map<int, int>::iterator itr=N_status.begin();itr!=N_status.end();itr++)
        {
            std::cout << "+" << itr->first << "sigma:  " << itr->first << std::endl;
        }
        std::cout << "Median:   " << med_status << std::endl;
        std::cout << "Injected: " << inj_status << std::endl;
        std::cout << "Observed: " << obs_status << std::endl;
        std::cout << "--------------------------------" << std::endl;
    }

    if (betterBands) std::cout << "Guess for bands" << std::endl;
    std::cout << "+2sigma:  " << mu_up_p2_approx << std::endl;
    std::cout << "+1sigma:  " << mu_up_p1_approx << std::endl;
    std::cout << "-1sigma:  " << mu_up_n1_approx << std::endl;
    std::cout << "-2sigma:  " << mu_up_n2_approx << std::endl;
    if (betterBands) {
        std::cout << std::endl;
        std::cout << "Correct bands" << std::endl;
        std::cout << "+2sigma:  " << mu_up_p2 << std::endl;
        std::cout << "+1sigma:  " << mu_up_p1 << std::endl;
        std::cout << "-1sigma:  " << mu_up_n1 << std::endl;
        std::cout << "-2sigma:  " << mu_up_n2 << std::endl;
    }

    std::cout << "Injected: " << inj_limit << std::endl;
    std::cout << "Median:   " << med_limit << std::endl;
    std::cout << "Observed: " << obs_limit << std::endl;
    std::cout << std::endl;

    system(("mkdir -vp " + folder).c_str());

    std::stringstream fileName;
    fileName << folder << "/" << mass << ".root";
    TFile fout(fileName.str().c_str(),"recreate");

    TH1D* h_lim = new TH1D("limit","limit",8,0,8);
    h_lim->SetBinContent(1, obs_limit);
    h_lim->SetBinContent(2, med_limit);
    h_lim->SetBinContent(3, mu_up_p2);
    h_lim->SetBinContent(4, mu_up_p1);
    h_lim->SetBinContent(5, mu_up_n1);
    h_lim->SetBinContent(6, mu_up_n2);
    h_lim->SetBinContent(7, inj_limit);
    h_lim->SetBinContent(8, global_status);

    h_lim->GetXaxis()->SetBinLabel(1, "Observed");
    h_lim->GetXaxis()->SetBinLabel(2, "Expected");
    h_lim->GetXaxis()->SetBinLabel(3, "+2sigma");
    h_lim->GetXaxis()->SetBinLabel(4, "+1sigma");
    h_lim->GetXaxis()->SetBinLabel(5, "-1sigma");
    h_lim->GetXaxis()->SetBinLabel(6, "-2sigma");
    h_lim->GetXaxis()->SetBinLabel(7, "Injected");
    h_lim->GetXaxis()->SetBinLabel(8, "Global status"); // do something with this later

    TH1D* h_lim_old = new TH1D("limit_old","limit_old",8,0,8); // include also old approximation of bands
    h_lim_old->SetBinContent(1, obs_limit);
    h_lim_old->SetBinContent(2, med_limit);
    h_lim_old->SetBinContent(3, mu_up_p2_approx);
    h_lim_old->SetBinContent(4, mu_up_p1_approx);
    h_lim_old->SetBinContent(5, mu_up_n1_approx);
    h_lim_old->SetBinContent(6, mu_up_n2_approx);
    h_lim_old->SetBinContent(7, inj_limit);
    h_lim_old->SetBinContent(8, global_status);

    h_lim_old->GetXaxis()->SetBinLabel(1, "Observed");
    h_lim_old->GetXaxis()->SetBinLabel(2, "Expected");
    h_lim_old->GetXaxis()->SetBinLabel(3, "+2sigma");
    h_lim_old->GetXaxis()->SetBinLabel(4, "+1sigma");
    h_lim_old->GetXaxis()->SetBinLabel(5, "-1sigma");
    h_lim_old->GetXaxis()->SetBinLabel(6, "-2sigma");
    h_lim_old->GetXaxis()->SetBinLabel(7, "Injected");
    h_lim_old->GetXaxis()->SetBinLabel(8, "Global status"); 

    fout.Write();
    fout.Close();

    std::cout << "Finished with " << nrMinimize << " calls to minimize(nll)" << std::endl;
}

RooNLLVar* createNLL(RooConfig* mc, RooDataSet* _data)
{
    const RooArgSet* nuis = mc->GetNuisanceParameters();

    RooNLLVar* nll;
    if ( nuis != 0 ) nll = (RooNLLVar*)mc->GetPdf()->createNLL(*_data, Constrain(*nuis), Optimize(2), Offset(true));
    else             nll = (RooNLLVar*)mc->GetPdf()->createNLL(*_data, Optimize(2), Offset(true));

    return nll;
}

RooDataSet* makeAsimovData(bool doConditional, RooNLLVar* conditioning_nll, double mu_val, string* mu_str, string* mu_prof_str, double mu_val_profile, bool doFit, double mu_injection)
{
    if (mu_val_profile == -999) mu_val_profile = mu_val;

    ////////////////////
    //make asimov data//
    ////////////////////
    RooAbsPdf* combPdf = mc->GetPdf();

    int _printLevel = 0;

    std::stringstream muStr;
    muStr << setprecision(5);
    muStr << "_" << mu_val;
    if (mu_str) *mu_str = muStr.str();

    std::stringstream muStrProf;
    muStrProf << setprecision(5);
    muStrProf << "_" << mu_val_profile;
    if (mu_prof_str) *mu_prof_str = muStrProf.str();

    RooRealVar* mu = (RooRealVar*)mc->GetParametersOfInterest()->first();
    mu->setVal(mu_val);

    RooArgSet mc_obs   = *mc->GetObservables();
    RooArgSet mc_globs = *mc->GetGlobalObservables();
    RooArgSet mc_nuis  = (mc->GetNuisanceParameters() ? *mc->GetNuisanceParameters() : RooArgSet());

    //pair the nuisance parameter to the global observable
    RooArgSet mc_nuis_tmp = mc_nuis;
    RooArgList nui_list("ordered_nuis");
    RooArgList glob_list("ordered_globs");
    RooArgSet constraint_set_tmp(*combPdf->getAllConstraints(mc_obs, mc_nuis_tmp, false));
    RooArgSet constraint_set;
    int counter_tmp = 0;
    unfoldConstraints(constraint_set_tmp, constraint_set, mc_obs, mc_nuis_tmp, counter_tmp);

    TIterator* cIter = constraint_set.createIterator();
    RooAbsArg* arg;
    while ((arg = (RooAbsArg*)cIter->Next())) {
        RooAbsPdf* pdf = (RooAbsPdf*)arg;
        if (!pdf) continue;
        std::cout << "Printing pdf" << std::endl;
        pdf->Print();
        std::cout << "Done" << std::endl;

        TIterator* nIter = mc_nuis.createIterator();
        RooRealVar* thisNui = NULL;
        RooAbsArg* nui_arg;
        while ((nui_arg = (RooAbsArg*)nIter->Next())) {
            if (pdf->dependsOn(*nui_arg)) {
                thisNui = (RooRealVar*)nui_arg;
                break;
            }
        }
        delete nIter;

        //need this incase the observable isn't fundamental. 
        //in this case, see which variable is dependent on the nuisance parameter and use that.
        RooArgSet* components = pdf->getComponents();
        //     cout << "\nPrinting components" << endl;
        //     components->Print();
        //     cout << "Done" << endl;
        components->remove(*pdf);
        if (components->getSize()) {
            TIterator* itr1 = components->createIterator();
            RooAbsArg* arg1;
            while ((arg1 = (RooAbsArg*)itr1->Next())) {
                TIterator* itr2 = components->createIterator();
                RooAbsArg* arg2;
                while ((arg2 = (RooAbsArg*)itr2->Next())) {
                    if (arg1 == arg2) continue;
                    if (arg2->dependsOn(*arg1)) {
                        components->remove(*arg1);
                    }
                }
                delete itr2;
            }
            delete itr1;
        }
        if (components->getSize() > 1) {
            cout << "ERROR::Couldn't isolate proper nuisance parameter" << endl;
            return NULL;
        }
        else if (components->getSize() == 1) {
            thisNui = (RooRealVar*)components->first();
        }

        TIterator* gIter = mc_globs.createIterator();
        RooRealVar* thisGlob = NULL;
        RooAbsArg* glob_arg;
        while ((glob_arg = (RooAbsArg*)gIter->Next())) {
            if (pdf->dependsOn(*glob_arg)) {
                thisGlob = (RooRealVar*)glob_arg;
                break;
            }
        }
        delete gIter;

        if (!thisNui || !thisGlob) {
            cout << "WARNING::Couldn't find nui or glob for constraint: " << pdf->GetName() << endl;
            //return;
            continue;
        }

        if (_printLevel >= 1) cout << "Pairing nui: " << thisNui->GetName() << ", with glob: " << thisGlob->GetName() << ", from constraint: " << pdf->GetName() << endl;

        nui_list.add(*thisNui);
        glob_list.add(*thisGlob);

        //     cout << "\nPrinting Nui/glob" << endl;
        //     thisNui->Print();
        //     cout << "Done nui" << endl;
        //     thisGlob->Print();
        //     cout << "Done glob" << endl;
    }
    delete cIter;

    // save the snapshots of nominal parameters, but only if they're not already saved
    w->saveSnapshot("tmpGlobs",*mc->GetGlobalObservables());
    w->saveSnapshot("tmpNuis",(mc->GetNuisanceParameters() ? *mc->GetNuisanceParameters() : RooArgSet()));
    if (!w->loadSnapshot("nominalGlobs")) {
        cout << "nominalGlobs doesn't exist. Saving snapshot." << endl;
        w->saveSnapshot("nominalGlobs",*mc->GetGlobalObservables());
    }
    else w->loadSnapshot("tmpGlobs");
    if (!w->loadSnapshot("nominalNuis")) {
        cout << "nominalNuis doesn't exist. Saving snapshot." << endl;
        w->saveSnapshot("nominalNuis",(mc->GetNuisanceParameters() ? *mc->GetNuisanceParameters() : RooArgSet()));
    }
    else w->loadSnapshot("tmpNuis");

    RooArgSet nuiSet_tmp(nui_list);

    mu->setVal(mu_val_profile);
    mu->setConstant(1);

    if (doConditional && doFit) { minimize(conditioning_nll); }
    mu->setConstant(0);
    mu->setVal(mu_val);

    //loop over the nui/glob list, grab the corresponding variable from the tmp ws, and set the glob to the value of the nui
    if (nui_list.getSize()!= glob_list.getSize()) {
        cout << "ERROR::nui_list.getSize() != glob_list.getSize()!" << endl;
        return NULL;
    }

    for (int i=0; i < nui_list.getSize();i++) {
        RooRealVar* nui = (RooRealVar*)nui_list.at(i);
        RooRealVar* glob = (RooRealVar*)glob_list.at(i);

        //cout << "nui: " << nui << ", glob: " << glob << endl;
        //cout << "Setting glob: " << glob->GetName() << ", which had previous val: " << glob->getVal() << ", to conditional val: " << nui->getVal() << endl;

        glob->setVal(nui->getVal());
    }

    //save the snapshots of conditional parameters
    // cout << "Saving conditional snapshots" << endl;
    // cout << "Glob snapshot name = " << "conditionalGlobs"+muStrProf.str() << endl;
    // cout << "Nuis snapshot name = " << "conditionalNuis"+muStrProf.str() << endl;
    w->saveSnapshot(("conditionalGlobs"+muStrProf.str()).c_str(),*mc->GetGlobalObservables());
    w->saveSnapshot(("conditionalNuis" +muStrProf.str()).c_str(),( mc->GetNuisanceParameters() ? *mc->GetNuisanceParameters() : RooArgSet()));

    if (!doConditional) {
        w->loadSnapshot("nominalGlobs");
        w->loadSnapshot("nominalNuis");
    }

    if (_printLevel >= 1) cout << "Making asimov" << endl;
    //make the asimov data (snipped from Kyle)
    mu->setVal(mu_val);

    if(mu_injection > 0) {
        RooRealVar* norm_injection = w->var("ATLAS_norm_muInjection");
        if(norm_injection) {
            norm_injection->setVal(mu_injection);
        }
        else {
            mu->setVal(mu_injection);
        }
    }

    int iFrame=0;

    const char* weightName="weightVar";
    RooArgSet obsAndWeight;
    //cout << "adding obs" << endl;
    obsAndWeight.add(*mc->GetObservables());
    //cout << "adding weight" << endl;

    RooRealVar* weightVar = NULL;
    if (!(weightVar = w->var(weightName))) {
        w->import(*(new RooRealVar(weightName, weightName, 1,0,10000000)));
        weightVar = w->var(weightName);
    }
    //cout << "weightVar: " << weightVar << endl;
    obsAndWeight.add(*w->var(weightName));

    //cout << "defining set" << endl;
    w->defineSet("obsAndWeight",obsAndWeight);


    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    // MAKE ASIMOV DATA FOR OBSERVABLES

    // dummy var can just have one bin since it's a dummy
    //if(w->var("ATLAS_dummyX"))  w->var("ATLAS_dummyX")->setBins(1);

    //cout <<" check expectedData by category"<<endl;
    //RooDataSet* simData=NULL;
    RooSimultaneous* simPdf = dynamic_cast<RooSimultaneous*>(mc->GetPdf());

    RooDataSet* asimovData;
    if (!simPdf) {
        // Get pdf associated with state from simpdf
        RooAbsPdf* pdftmp = mc->GetPdf();//simPdf->getPdf(channelCat->getLabel()) ;

        // Generate observables defined by the pdf associated with this state
        RooArgSet* obstmp = pdftmp->getObservables(*mc->GetObservables()) ;

        if (_printLevel >= 1)
        {
            obstmp->Print();
        }

        asimovData = new RooDataSet(("asimovData"+muStr.str()).c_str(),("asimovData"+muStr.str()).c_str(),RooArgSet(obsAndWeight),WeightVar(*weightVar));

        RooRealVar* thisObs = ((RooRealVar*)obstmp->first());
        double expectedEvents = pdftmp->expectedEvents(*obstmp);
        double thisNorm = 0;
        for(int jj=0; jj<thisObs->numBins(); ++jj){
            thisObs->setBin(jj);

            thisNorm=pdftmp->getVal(obstmp)*thisObs->getBinWidth(jj);
            if (thisNorm*expectedEvents <= 0)
            {
                cout << "WARNING::Detected bin with zero expected events (" << thisNorm*expectedEvents << ") ! Please check your inputs. Obs = " << thisObs->GetName() << ", bin = " << jj << endl;
            }
            if (thisNorm*expectedEvents > 0 && thisNorm*expectedEvents < pow(10.0, 18)) asimovData->add(*mc->GetObservables(), thisNorm*expectedEvents);
        }

        if (_printLevel >= 1)
        {
            asimovData->Print();
            cout <<"sum entries "<<asimovData->sumEntries()<<endl;
        }
        if(asimovData->sumEntries()!=asimovData->sumEntries()){
            cout << "sum entries is nan"<<endl;
            exit(1);
        }

        //((RooRealVar*)obstmp->first())->Print();
        //cout << "expected events " << pdftmp->expectedEvents(*obstmp) << endl;

        w->import(*asimovData);

        if (_printLevel >= 1)
        {
            asimovData->Print();
            cout << endl;
        }
    } else {
        map<string, RooDataSet*> asimovDataMap;

        //try fix for sim pdf
        RooCategory* channelCat = (RooCategory*)&simPdf->indexCat();//(RooCategory*)w->cat("master_channel");//(RooCategory*) (&simPdf->indexCat());
        //    TIterator* iter = simPdf->indexCat().typeIterator() ;
        TIterator* iter = channelCat->typeIterator() ;
        RooCatType* tt = NULL;
        int nrIndices = 0;
        while((tt=(RooCatType*) iter->Next())) {
            nrIndices++;
        }
        for (int i=0;i<nrIndices;i++){
            channelCat->setIndex(i);
            iFrame++;
            // Get pdf associated with state from simpdf
            RooAbsPdf* pdftmp = simPdf->getPdf(channelCat->getLabel()) ;

            // Generate observables defined by the pdf associated with this state
            RooArgSet* obstmp = pdftmp->getObservables(*mc->GetObservables()) ;

            if (_printLevel >= 1) {
                obstmp->Print();
                cout << "on type " << channelCat->getLabel() << " " << iFrame << endl;
            }

            RooDataSet* obsDataUnbinned = new RooDataSet(Form("combAsimovData%d",iFrame),Form("combAsimovData%d",iFrame),RooArgSet(obsAndWeight,*channelCat),WeightVar(*weightVar));
            RooRealVar* thisObs = ((RooRealVar*)obstmp->first());
            double expectedEvents = pdftmp->expectedEvents(*obstmp);
            double thisNorm = 0;
            for(int jj=0; jj<thisObs->numBins(); ++jj){
                thisObs->setBin(jj);

                thisNorm=pdftmp->getVal(obstmp)*thisObs->getBinWidth(jj);
                if (thisNorm*expectedEvents > 0 && thisNorm*expectedEvents < pow(10.0, 18)) obsDataUnbinned->add(*mc->GetObservables(), thisNorm*expectedEvents);
            }

            if (_printLevel >= 1) {
                obsDataUnbinned->Print();
                cout <<"sum entries "<<obsDataUnbinned->sumEntries()<<endl;
            }
            if(obsDataUnbinned->sumEntries()!=obsDataUnbinned->sumEntries()){
                cout << "sum entries is nan"<<endl;
                exit(1);
            }

            // ((RooRealVar*)obstmp->first())->Print();
            // cout << "pdf: " << pdftmp->GetName() << endl;
            // cout << "expected events " << pdftmp->expectedEvents(*obstmp) << endl;
            // cout << "-----" << endl;

            asimovDataMap[string(channelCat->getLabel())] = obsDataUnbinned;//tempData;

            if (_printLevel >= 1) {
                cout << "channel: " << channelCat->getLabel() << ", data: ";
                obsDataUnbinned->Print();
                cout << endl;
            }
        }

        asimovData = new RooDataSet(("asimovData"+muStr.str()).c_str(),("asimovData"+muStr.str()).c_str(),RooArgSet(obsAndWeight,*channelCat),Index(*channelCat),Import(asimovDataMap),WeightVar(*weightVar));
        w->import(*asimovData);
    }

    if(mu_injection > 0) {
        RooRealVar* norm_injection = w->var("ATLAS_norm_muInjection");
        if(norm_injection) {
            norm_injection->setVal(0);
        }
    }

    //bring us back to nominal for exporting
    w->loadSnapshot("nominalGlobs");

    return asimovData;
}
