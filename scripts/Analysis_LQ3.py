#!/usr/bin/env python
# For combined need to scale both to same input CR first
"""
    This is the main drier for running taskas (limits, pulls, rankings etc).
    The core script is the WSMakerCore/scripts/doActions.py, thus all this script can do is to call the doActions.py script with the correct 
    arguments. These arugments can be created by the arguments given to this script.

    Import the AnalysisMgr_XXX and create the config files based on the input and output version names which will be given as the argument.

"""

import sys
import os
from argparse import ArgumentParser
import AnalysisMgr_LQ3 as Mgr
import BatchMgr 

def add_config(
        mass,
        unc          = "Systs",
        mcstats      = True, 
        one_bin      = False, 
        chan         = "lephad", 
        UseBDTScore  = False, 
        UseBDTScoreSingle  = False, 
        UsePNNScore  = False, 
        UseSingleVar = False, 
        one_tag      = False, 
        plots        = False, 
        plot_region  = ""
        ):

    conf = Mgr.WorkspaceConfig_LQ3(
            unc,
            InputVersion            = InputVersion, 
            Type                    = "LQ3", 
            MassPoint               = mass, 
            OneBin                  = one_bin, 
            DoInjection             = 0,
            UseStatSystematics      = mcstats, 
            StatThreshold           = 0.00, 
            Debug                   = True, 
            DoShapePlots            = True,
            DoSystsPlots            = True, 
            LogScalePlots           = True,
            ShowMCStatBandInSysPlot = True,
            SmoothingControlPlots   = True, 
            Postfit = True if plots else False, 
            LTT = False)
    
    #conf.set_channels(channel)

    conf.set_regions(
            channel      = chan, 
            mass         = mass, 
            one_tag      = one_tag, 
            UseBDTScore  = UseBDTScore, 
            UsePNNScore  = UsePNNScore, 
            UseSingleVar = UseSingleVar, 
            plots        = plots, 
            plot_region  = plot_region, 
            )

    fullversion = "LQ3_13TeV_{}_{}_{}".format( outversion, unc, chan ) 

    if UseBDTScore:       fullversion += "_BDT"        
    if UsePNNScore:       fullversion += "_"+plot_region +"_PNN"
    if UseBDTScoreSingle: fullversion += "_single_trained"        
    elif UseSingleVar :   fullversion += "_" + UseSingleVar

    fullversion += "_" + mass
    return conf.write_configs(fullversion)

if __name__ == "__main__":

    # Command line argument parser
    usage = 'Usage: Analysis_LQ3.py <outversion>'
    argparser = ArgumentParser(usage=usage)
    argparser.add_argument('-i'  , '--inputVersion' , type=str , dest='InputVersion' , help='echo fname')
    argparser.add_argument('-o'  , '--outputVersion', type=str , dest='OutputVersion', help='echo fname')
    argparser.add_argument('-c'  , '--channel'      , type=str , dest='channel'      , help='echo fname', default="lephad")
    argparser.add_argument('-lq3', '--lq3'          , type=bool, dest='lq3'          , help='echo fname', default=True)

    # flags
    argparser.add_argument('--use_bdt'   , action='store_true', help='echo fname')
    argparser.add_argument('--use_bdt_single'   , action='store_true', help='echo fname')
    argparser.add_argument('--use_pnn'   , action='store_true', help='echo fname')
    argparser.add_argument('--do_limits' , action='store_true', help='echo fname')
    argparser.add_argument('--do_ranking', action='store_true', help='echo fname')
    argparser.add_argument('--do_fcc'    , action='store_true', help='echo fname')
    argparser.add_argument('--do_pulls'  , action='store_true', help='echo fname')
    argparser.add_argument('--do_tables' , action='store_true', help='echo fname')
    argparser.add_argument('--do_postfit', action='store_true', help='echo fname')

    args = argparser.parse_args()

    # Argument handling
    InputVersion = args.InputVersion
    outversion   = args.OutputVersion
    chan         = args.channel
    lq3          = args.lq3

    if not args.InputVersion or not args.channel or not args.OutputVersion:
        print usage
        exit(0)

    # What to run ...
    doplots = False #"BDTCRs"
    dozcr   = False

    ########## create config files
    print ">>>>>>>> Create config files ..."
    configfiles_stat = []
    configfiles = []

    masses = [300,400,500,600,700,800,900,950,1000,1050,1100,1150,1200,1250,1350,1400,1450,1500,1550,1600,1700,1800,1900,2000]
    masses = [300,]

    single_vars = ( "BasicKinematics_Preselected_TauPT", )
    Cuts     = ( 'config_fullRun2', )
    cuts_pnn = ( 'PNN',  )

    # Can be difficult to converge unless multiply small signal
    # (done in systematicslistbuilder_hh but only for syst/float limits -> hack to use FloatOnly here and add a return in the function)
    if args.use_bdt :
        for m in masses:
            for Cut in Cuts:
                file_suffix = 'BDTScore_' + Cut
                configfiles_stat += add_config(str(m), "StatOnly", UseBDTScore = True, plots = doplots, one_tag = False, plot_region = file_suffix + "_SR_LQ3")  
    elif args.use_pnn :
        for m in masses:
            for epochs in (10, 100, 500, 1000):
              for batch_size in (10, 64, 100):
                for learning_rate in ("0p01", "0p1", "0p5", "0p9"):
                  for learning_rate_decay in ("1ep05",):
                    for layer_size in ("32p32", "32p32p32", "32p32p32p32", "100p100p100"):
                        file_suffix = 'PNNScore_epochs{}_batchsize{}_rate{}_decay{}_layer{}'.format(epochs, batch_size, learning_rate, learning_rate_decay, layer_size)
                        configfiles_stat += add_config(str(m), "StatOnly", UsePNNScore = True, plots = doplots, one_tag = False, plot_region = file_suffix + "_SR_LQ3")  
    elif args.use_bdt_single :
        for m in masses:
            for Cut in Cuts:
                file_suffix = 'BDTScore_single_trained_' + Cut
                configfiles_stat += add_config(str(m), "Syst", UseBDTScore = True, UseBDTScoreSingle = True, plots = doplots, one_tag = False, plot_region = file_suffix + "_SR")  
    else :    
        for single_var in single_vars:
            for m in masses:
                file_suffix = single_var
                configfiles_stat += add_config(str(m), file_suffix, UseBDTScore = False, UseSingleVar = single_var, plots = doplots, one_tag = False)  
                configfiles      += add_config(str(m), "Systs"    , UseBDTScore = False, UseSingleVar = single_var, plots = doplots, one_tag = False)  

    print "Created config files (./config): "
    for fi in configfiles_stat:
        print " - " + fi
    for fi in configfiles:
        print " - " + fi
    print ">>>>>>>> Succeeded to create the config files"

    ########## 
    # Create the optinos for doActions.py

    # create workspace (do always)
    options = ["-w"]             

    if args.do_limits:
        options += ["-l", "0,{MassPoint}"]        

    if args.do_tables or args.do_postfit or args.do_fcc:
        options += ["--fcc", "4,10@{MassPoint}"]

    if args.do_tables:
        options += ["-t", "1@{MassPoint}"]

    if args.do_postfit:
        options += ["-p", "1@{MassPoint}"]

    if args.do_pulls:
        options += ["--fcc", "2,7@{MassPoint}"]

    if args.do_ranking:
        options += ["-r", "{{MassPoint}}"]
        options += ["-n", "{MassPoint}"]        

    ########## run jobs locally
    BatchMgr.run_local_batch(configfiles     , outversion, options)    
    BatchMgr.run_local_batch(configfiles_stat, outversion, options)
