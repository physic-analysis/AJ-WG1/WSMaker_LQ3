
import sys
import ROOT as R
from array import array
from collections import OrderedDict as odict
import LQ3AtlasStyle as astyle
import json
import os
import pandas as pd
import numpy as np
from LimitPlotter import LimitPlotter
    
if __name__ == '__main__':

    R.gROOT.SetBatch(True)
    plotter = LimitPlotter('pp13_stopsbottom_NNLO+NNLL.json')

    # ATLAS style 
    astyle.SetAtlasStyle()


    # Error band 
    #plotter.PlotExpectedCurveErrorBand( name_bdt , R.kMagenta, legend="Exp 139 fb^{-1} (BDT)", beta1_weight = True)

    # Theory curve
    plotter.PlotTheoryCurve(draw_error_band = True)
    
#    colors = (R.kBlue, R.kRed, R.kBlack)
#    for index, btag in enumerate(('1tag','2tag','1p2tag')):
#        name_bdt = 'LQ3LH_v7.' + btag + '_LQ3_13TeV_' + btag + '_StatOnly_lephad_BDT_{}'
#        name_pnn = 'LQ3LH_v7.' + btag + '_LQ3_13TeV_' + btag + '_StatOnly_lephad_PNNScore_epochs100_batchsize64_rate0p1_decay1ep05_layer32p32p32_SR_LQ3_PNN_{}'
#        plotter.PlotExpectedCurve( name_bdt, color=colors[index], linestyle=7, legend="Exp 139 fb^{-1} (BDT)", beta1_weight = True, scan = True)
#        plotter.PlotExpectedCurve( name_pnn, color=colors[index], linestyle=1, legend="Exp 139 fb^{-1} (PNN)", beta1_weight = True, scan = True)
#        
#    plotter.PlotExpectedCurve( 'LQ3LH_v1.output_LQ3_13TeV_output_StatOnly_lephad_LQ3_BDT_{}', color=R.kGreen,legend="Exp 139 fb^{-1} (PNN)", beta1_weight = True, scan = True)

    name = 'LQ3LH_v8_prevNBJet_priorHighPt.output_LQ3_13TeV_output_StatOnly_lephad_BDT_{}'
    #plotter.PlotExpectedCurve(name, color=R.kGreen,legend="Exp 139 fb^{-1} (PNN111)", beta1_weight = True)
    name = 'LQ3LH_v8_newNBJet_priorHighPt.output_LQ3_13TeV_output_StatOnly_lephad_BDT_{}'
    plotter.PlotExpectedCurve(name, color=R.kRed,legend="Exp 139 fb^{-1} (PNN)", beta1_weight = True, scan = True)
   
    astyle.ATLASLabel(0.42,0.85,'Simulation Work in progress', plotter.GetCanvas())
    plotter.SaveCanvas()

    sys.exit()
