
from AnalysisMgr import WorkspaceConfig

class WorkspaceConfig_LQ3(WorkspaceConfig):
    def __init__(self, *args, **kwargs):
        kwargs["Analysis"] = "LQ3"
        super(WorkspaceConfig_LQ3, self).__init__(*args, **kwargs)
        return

    def set_regions(
            self, 
            channel      = "lephad", 
            mass         = None, 
            one_tag      = False, 
            UseBDTScore  = False, 
            UsePNNScore  = False, 
            UseSingleVar = False,
            plots        = False, 
            plot_region  = ""):

        self["Regions"] = []

        """
        Define the discriminating input variables.
        Default values (for previous publication) are the BDT output scores.
        """
        variables = []
        if plots:
            if channel == "lephad":
                variables = plots if isinstance(plots, list) else ["MET", "MtW", "dPhiHBB", "Mhh", "mbb", "METCent", "pTB2", "dPtLepTau", "dRbb", "DRTauTau", "mMMC"]
                for var in variables :
                    print (" -- " + str(var))

        # Calculate the limit on the BDT score
        elif UseBDTScore:
            variables = [ "BDT_{}".format(mass) ]
            
        # Calculate the limit on the PNN score
        elif UsePNNScore:
            variables = [ "PNN_{}".format(mass)]

        # Calculate the limit on the single variable
        elif UseSingleVar:
            if channel == "lephad":
                print UseSingleVar
                self["Regions"] += [ 
                        "13TeV_TauLH_1tag2pjet_0ptv_" + UseSingleVar,
                        "13TeV_TauLH_2tag2pjet_0ptv_" + UseSingleVar,
                        ]
                variables =[] 
        else:
            print("Please define what is the discriminating variables.")
            exit(1)

        """
          Create the region name  
          Regions : the intput ROOT file name (inputs/)
        """
        for var in variables:
            print(">>>>>>>>>> var=" + var)
            if channel == "lephad" :            
                if UseSingleVar:
                    print("plot_region = {}".format(plot_region) )
                    if one_tag : 
                        self["Regions"] += [ "13TeV_TauLH_1tag2pjet_0ptv_{}".format(var), ]
                    else : 
                        self["Regions"] += [ "13TeV_TauLH_1tag2pjet_0ptv_{}".format(var), ]
                        self["Regions"] += [ "13TeV_TauLH_2tag2pjet_0ptv_{}".format(var), ]
                    print(self["Regions"])
                else:
                    print("plot_region = {}".format(plot_region) )
                    if one_tag : 
                        self["Regions"] += [ "13TeV_TauLH_1tag2pjet_0ptv_{}_{}".format(plot_region,var), ]
                    else : 
                        self["Regions"] += [ "13TeV_TauLH_1tag2pjet_0ptv_{}_{}".format(plot_region,var), ]
                        self["Regions"] += [ "13TeV_TauLH_2tag2pjet_0ptv_{}_{}".format(plot_region,var), ]
                    print(self["Regions"])
                    
        self.check_regions()
